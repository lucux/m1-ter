# Méthodes de classification pour des données législatives et judiciaires.

La classification automatique ou clustering consiste à partitionner un ensemble d’objets (instances) décrits par un ensemble de variables en groupes (classes) homogènes. Avec l’avènement du Big Data et de la science des données, le clustering est devenu une tache encore plus importante.

La justice est un domaine produisant une importante masse de données textuelles non structurées conformément à la constitution, la légalisation et la jurisprudence des cours et des tribunaux. Dans ce contexte, on recense plusieurs bases de données regroupant les différentes lois établies, les arrêtés, les décisions de justice prises en matière civile et pénal, et bien plus encore.

## Organisation du code

* CreationMatrice 
      * Tous le code pour créer la matrice (bash) (de la récupération des données depuis DataGouv, à leurs traitement pour en extraire pour chaque documents leurs articles (de codes, de lois, de conventions européennes et internationales,de decrets et d'ordonnances). Ensuite pour créer la matrice via python.
      * downloadListCASS: liste des liens des tar pour cass
      * cassTar: tous les tar
      * cassDir: tous les répertoires 
      * cassXML: tous les fichiers xml des répertoires
      * xml_cass: répertoire du traitement des fichiers xml
          - Pre-traitement(on parcourt les fichiers xml):
            1. ref/: 
                * code.csv: liste des codes français
                * europ: liste des conventions européennes
                * conventionsUN: liste des conventions internationales (Nations Unies)
            2. pickpick(xml_pickerX): on prend ce dont on a besoin dans les fichiers et on enleve balises HTML, les citations, les phrases entre parentheses, les deux point, les crochets, et point virgules.
            3. noAccentLower (xml_ok(p)X): parcourt les fichiers xml, enleve les accents et met tous les caracteres en minuscule.
            4. python tokenise.py: crée les datas (data/)
                * liste des articles  #article
                * liste des articles inversés #articleI
                * liste des documents #doc
                * liste des documents inversés #docI
                * liste des occurences #occur
                * la matrice #dtm
          - On utilise Python pour créer la matrice sparse (ipython notebook) (data/matrixJade.mat)
* TravailSurMatrice
      * data
         * Tous les .Rdata .RHistory .Rmd sont par Rstudio
      * graphs
         * Tous les graphs (wss et silhouette) avec et sans TFIDF pour kmeans, kmeans avec khi2, skmeans et CAH avec le critère Ward.  