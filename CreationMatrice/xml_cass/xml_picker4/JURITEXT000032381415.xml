LA COUR DE CASSATION, CHAMBRE CRIMINELLE, a rendu l'arrêt suivant  
Statuant sur les pourvois formés par 
- M. Gilles X...,- Mme Annie Y..., épouse Z..., 
contre l'arrêt de la cour d'appel de CHAMBÉRY, chambre correctionnelle, en date du 14 janvier 2015, qui, pour offre d'adhésion à une chaîne faisant espérer des gains financiers par la progression géométrique des adhérents, a condamné le premier, à dix mois d'emprisonnement avec sursis et 4 500 euros d'amende, la seconde, à six mois d'emprisonnement avec sursis et 4 500 euros d'amende, et a prononcé des mesures de confiscation  
La COUR, statuant après débats en l'audience publique du 17 février 2016 où étaient présents dans la formation prévue à l'article 567-1-1 du code de procédure pénale  M. Guérin, président, Mme Pichon, conseiller rapporteur, M. Soulard, conseiller de la chambre  
Greffier de chambre  Mme Randouin  
Sur le rapport de Mme le conseiller référendaire PICHON, les observations de la société civile professionnelle WAQUET, FARGE et HAZAN, avocat en la Cour, et les conclusions de M. l'avocat général GAILLARDOT  
Joignant les pourvois en raison de la connexité  
Vu le mémoire produit, commun aux demandeurs  
Sur le premier moyen de cassation, pris de la violation des articles 6 de la Convention européenne des droits de l'homme, 406, 512, 591 et 593 du code de procédure pénale  
" en ce que l'arrêt attaqué a été rendu sans que les prévenus se soient vu régulièrement notifier leur droit de se taire  
" aux motifs que le président a constaté l'absence de MM. Jean-Claude A..., Dominique B..., Jean-Michel B..., Jean-François B... et l'identité des autres prévenus et leur a donné connaissance des dispositions de l'article 406 du code de procédure pénale  
<FIN>  
Attendu qu'il résulte des mentions de l'arrêt attaqué que le président a donné connaissance aux prévenus, M. X... et Mme Y..., comparants et assistés de leurs avocats, des dispositions de l'article 406 du code de procédure pénale  
D'où il suit que le moyen doit être écarté  
Sur le deuxième moyen de cassation, pris de la violation des articles L. 122-6, 2°, du code de la consommation, 121-3 du code pénal, 459, 485, 591 et 593 du code de procédure pénale, défaut de motifs, manque de base légale  
" en ce que l'arrêt confirmatif attaqué a déclaré les prévenus coupables d'offre d'adhésion à une chaîne faisant espérer des gains financiers par la progression géométrique des adhérents  
" aux motifs que, pour que le délit de l'article L. 122-6 du code de la consommation soit constitué, il suffit d'une proposition d'inscription sur une liste ou d'adhésion à une opération laissant espérer des gains financiers moyennant une participation financière, fonction de la progression du nombre de personnes recrutées ou inscrites  qu'il est établi qu'hormis M. Dominique B..., tous les prévenus ont admis ou sont convaincus d'avoir eu un rôle actif dans la recherche de nouveaux adhérents en organisant ou en participant à des réunions à leur domicile ou dans une salle du restaurant La Tour de Pacoret ou dans la cafétéria des consorts B..., en recrutant des personnes, souvent des proches, les incitant à entrer dans ce processus et leur faisant espérer un gain financier important  que les déclarations des différentes personnes entendues dans le cadre de cette procédure sont cohérentes en ce qu'elles décrivent toutes le rôle actif des prévenus lors des réunions auxquelles elles ont participé  que les prévenus ne peuvent, dès lors, nier avoir eu un rôle actif dans la recherche de nouvelles adhésions, élément moteur et déterminant dans le fonctionnement d'un tel processus  
<FIN>  
Attendu que, pour déclarer les demandeurs coupables du délit d'offre d'adhésion à une chaîne faisant espérer des gains financiers par la progression géométrique des adhérents, l'arrêt énonce qu'ils ont admis avoir eu un rôle actif dans la recherche de nouveaux adhérents à une telle chaîne en organisant ou en participant à des réunions, en recrutant des personnes, les incitant à entrer dans le processus et leur faisant espérer un gain financier important  
Attendu qu'en l'état de ces énonciations, et dès lors que la caractérisation de l'élément intentionnel de cette infraction n'est pas subordonnée à la preuve de la conscience qu'ont les prévenus, au moment où ils proposent à des tiers d'adhérer, du caractère préjudiciable du système pyramidal, mais suppose seulement que soit établie leur volonté de proposer une telle adhésion en faisant espérer à ces tiers un gain financier qui résulterait de la progression du nombre d'adhérents, la cour d'appel a justifié sa décision  
D'où il suit que le moyen ne saurait être accueilli  
Mais sur le troisième moyen de cassation, pris de la violation des articles 7, § 1, de la Convention européenne des droits de l'homme, 8 de la Déclaration des droits de l'homme et du citoyen, 111-3, 112-1 et 131-21 du code pénal, L. 122-6, 2°, et L. 122-7 du code de la consommation, 591 du code de procédure pénale  
" en ce que l'arrêt confirmatif attaqué a ordonné la confiscation des sommes saisies  
" aux motifs que, eu égard à la gravité des faits, les peines d'emprisonnement prononcées par le premier juge, parfaitement adaptées à la personnalité de chacun des prévenus et à leur implication dans le processus illicite auquel ils ont participé et qu'ils ont alimenté, seront confirmées de même que les mesures de confiscation  
<FIN>  
Vu l'article 112-1 du code pénal  
Attendu que seules peuvent être prononcées les peines légalement applicables à la date à laquelle les faits constitutifs d'une infraction ont été commis  
Attendu qu'après avoir déclaré les prévenus coupables du délit reproché, l'arrêt les a condamnés, notamment, à des peines complémentaires de confiscation  
Mais attendu qu'en prononçant ainsi, alors qu'à l'époque de la commission des faits, antérieure à l'entrée en vigueur de la loi n° 2014-344 du 17 mars 2014, le délit prévu et réprimé par les articles L. 122-6 et L. 122-7 du code de la consommation n'était pas puni d'une peine d'emprisonnement d'une durée supérieure à une année, mais seulement d'une année, et que la peine complémentaire de confiscation, prévue à l'article 131-21 du code pénal, n'était donc pas encourue, la cour d'appel a méconnu le texte susvisé et le principe ci-dessus énoncé  
D'où il suit que la cassation est encourue de chef  
Par ces motifs  
CASSE et ANNULE, par voie de retranchement, l'arrêt susvisé de la cour d'appel de Chambéry, en date du 14 janvier 2015, en ses seules dispositions relatives aux peines de confiscation, toutes autres dispositions étant expressément maintenues  
DIT n'y avoir lieu à renvoi  
ORDONNE l'impression du présent arrêt, sa transcription sur les registres du greffe de la cour d'appel de Chambéry et sa mention en marge ou à la suite de l'arrêt partiellement annulé  
Ainsi fait et jugé par la Cour de cassation, chambre criminelle, et prononcé par le président le six avril deux mille seize  
En foi de quoi le présent arrêt a été signé par le président, le rapporteur et le greffier de chambre.
