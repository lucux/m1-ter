LA COUR DE CASSATION, CHAMBRE CRIMINELLE, a rendu l'arrêt suivant  
Statuant sur le pourvoi formé par 
- M. Mukhtar X..., 
contre l'arrêt n° 882 de la chambre de l'instruction de la cour d'appel de LYON, en date du 5 juillet 2016, qui, sur renvoi après cassation (Crim., 8 juin 2016, n° 16-81. 912), dans la procédure d'extradition suivie contre lui à la demande du gouvernement ukrainien, a rejeté sa demande de mise en liberté  
Vu le mémoire et les observations complémentaires produits  
Sur le premier moyen de cassation, pris de la violation des articles 2, 4, 7 et 9 de la Déclaration des droits de l'homme et du citoyen, 66 de la Constitution, 5 de la Convention européenne des droits de l'homme  
" en ce que l'arrêt attaqué a rejeté la demande de mise en liberté de M. 
X...
, placé sous écrou extraditionnel  
" 1°) alors que les dispositions des articles 696-11 et 696-19 du code de procédure pénale portent atteinte aux droits et libertés garantis par les articles 2, 4, 7 et 9 de la Déclaration des droits de l'homme et du citoyen et 66 de la Constitution  qu'en conséquence de la déclaration d'inconstitutionnalité des textes précités qui sera prononcée sur la question prioritaire de constitutionnalité en cours d'examen au Conseil constitutionnel, l'arrêt attaqué se trouve privé de tout fondement légal  
" 2°) alors que les articles 696-11 et 696-19 du code de procédure pénale n'organisent pas les droits de la défense au stade du placement initial sous écrou extraditionnel décidé par le premier président de la cour d'appel ou son délégué, ne confèrent au contrôle judiciaire et à l'assignation à résidence qu'un caractère subsidiaire par rapport à la détention, n'instituent qu'un recours contre la décision de placement sous contrôle judiciaire ou d'assignation à résidence et ne fixent aucune limite à la durée de l'incarcération  qu'en outre, et bien que le placement initial sous écrou extraditionnel soit intervenu sans que les droits de la défense aient été garantis, ce qui devrait justifier une célérité accrue du contrôle de cette décision, la chambre de l'instruction, qui peut uniquement être saisie dans le cadre d'une demande de mise en liberté, ne statue que dans un délai de quinze à vingt jours  qu'ainsi, le placement et le maintien sous écrou extraditionnel de M. 
X...
n'ont pas été « réguliers » au sens de l'article 5, § 1, de la Convention européenne des droits de l'homme et celui-ci n'a pu bénéficier d'un contrôle de son placement sous écrou extraditionnel à bref délai au sens de l'article 5, § 4, de la Convention européenne des droits de l'homme "  
Sur le moyen, pris en sa première branche  
Attendu que les dispositions des articles 696-11 et 696-19 du code de procédure pénale ayant été déclarées conformes à la Constitution par décision du Conseil constitutionnel n° 561/ 562 QPC en date du 9 septembre 2016, le moyen, pris en sa première branche, est inopérant  
Sur le moyen, pris en sa seconde branche 
 Attendu que le moyen, qui critique la procédure initiale par laquelle M. 
X...
a été placé sous écrou extraditionnel, est irrecevable pour ne pas avoir été proposé devant la chambre de l'instruction  
D'où il suit que le moyen ne peut qu'être écarté  
Sur le second moyen de cassation, pris de la violation des articles 9 de la Déclaration des droits de l'homme et 66 de la Constitution, 5-1 f) et 8 de la Convention européenne des droits de l'homme, préliminaire et 593 du code de procédure pénale, défaut de motifs, manque de base légale  
" en ce que l'arrêt attaqué a rejeté la demande de mise en liberté de M. 
X...
, placé sous écrou extraditionnel  
" aux motifs qu'il n'est pas contestable que depuis l'arrêt de la chambre criminelle, en date du 4 mars 2015, rejetant le pourvoi formé contre l'arrêt de la chambre de l'instruction du 24 octobre 2014 ayant émis un avis favorable à la demande d'extradition des autorités ukrainiennes, aucun décret n'est intervenu  qu'il est dénoncé par M. 
X...
les carences manifestes de l'autorité judiciaire et du pouvoir exécutif illustrées par la transmission tardive d'une pièce, par un renvoi sollicité par le parquet général, par le temps de traduction des décisions de justice pour notification en russe à M. 
X...
, par un délai de presque cinq mois entre le placement sous écrou extraditionnel et l'examen de la demande, par les irrégularités ayant affecté les décisions de la chambre de l'instruction  que cependant, il doit être rappelé que - que M. 
X...
a été placé sous écrou extraditionnel le 1er août 2013 - que dès le 9 janvier 2014, un avis favorable était émis par la chambre de l'instruction d'Aix-en-Provence après un renvoi de l'audience du 5 décembre à celle du 12 décembre 2013, soit dans un délai de huit jours - que sur pourvoi de M. 
X...
ledit arrêt a été cassé par un arrêt de la chambre criminelle de la cour de cassation du 9 avril 2014, la cause et les parties étant renvoyées devant la chambre de l'instruction de Lyon - que, par arrêt de la chambre de l'instruction de Lyon du 3 juin 2014 une question prioritaire de constitutionnalité déposée par l'intéressé n'a pas été transmise à la Cour de cassation - que, par arrêt de la chambre de l'instruction de Lyon du 12 juin 2014 un complément d'information a été ordonné à la demande de M. 
X...
aux fins de traduction des textes de répression applicables - que, par arrêt de la chambre de l'instruction de Lyon du 24 octobre 2014 un avis favorable sous réserve a été donné à son extradition - que M. 
X...
a alors formé un pourvoi en Cassation, rejeté par un arrêt de la chambre criminelle de la Cour de cassation du 4 mars 2015  que de la chronologie ininterrompue des diverses étapes du déroulement de la phase judiciaire de la procédure ne se déduisent ni carences ni retards, au vu notamment de la complexité de la demande d'extradition, des nécessités de traduction et de l'usage légitime par M. 
X...
de voies de recours  que l'absence de décret d'extradition du premier ministre sur la demande des autorités ukrainiennes ne résulte pas davantage de carence ou de retard dans le traitement de cette demande mais de l'existence du décret d'extradition vers la Fédération de Russie, désignée comme prioritaire par l'arrêt de la chambre de l'instruction  que cette décision fait obstacle, selon les termes même du mémoire, à la remise à l'Ukraine  que la procédure d'extradition est donc toujours en cours  que les autorités françaises ont conduit avec une diligence suffisante la complexe procédure d'extradition, procédure d'extradition qui n'a connu aucun retard important imputable aux autorités judiciaires et au pouvoir exécutif, dans son traitement  que la durée de la détention provisoire n'a pas excédé le délai raisonnable nécessaire pour atteindre le but visé à l'article 5, § 1, f de la Convention européenne des droits de l'homme  
" 1°) alors que seul le déroulement de la procédure d'extradition justifie la privation de liberté  que, par arrêt définitif du 24 octobre 2014, la chambre de l'instruction de la cour d'appel de Lyon a émis un avis favorable à la demande d'extradition formée par l'Ukraine ainsi que, par arrêt définitif du même jour, à la demande d'extradition parallèlement formée par la Fédération de Russie, avec priorité accordée à la remise de M. 
X...
à la Russie  que le gouvernement a consécutivement pris un décret d'extradition en faveur de la Russie le 17 septembre 2015  que la décision de la chambre de l'instruction en faveur de la Russie faisant obstacle à la remise de M. 
X...
aux autorités ukrainiennes, le maintien de ce dernier sous écrou extraditionnel dans le cadre de la demande ukrainienne n'est plus justifié par les strictes nécessités de la présente procédure  que la cassation interviendra sans renvoi et avec remise en liberté immédiate s'il n'est détenu pour autre cause  
" 2°) alors que la privation de liberté d'une personne placée sous écrou extraditionnel en vue d'être jugée par un Etat étranger pour des faits de nature correctionnelle, pendant plus de trois ans, soit une durée qui excède le maximum de la détention provisoire autorisée sur le territoire français pour ce type de faits, constitue, quel que soit l'objectif d'assurer la représentation de l'intéressé, une rigueur non nécessaire au sens de l'article 9 de la Déclaration des droits de l'homme et du citoyen et une privation de liberté d'une durée excessive au sens de l'article 5, § 1, f) de la Convention européenne des droits de l'homme  
" 3°) alors que si la procédure d'extradition n'est pas menée par les autorités avec la diligence requise, la détention cesse d'être justifiée au regard de l'article 5, § 1, f) de la Convention européenne des droits de l'homme  que M. 
X...
est placé sous écrou extraditionnel et privé de liberté depuis le 1er août 2013  qu'un délai de plus d'un an s'est écoulé depuis l'arrêt de la Cour de cassation du 4 mars 2015 ayant rejeté le pourvoi formé contre l'avis favorable de la chambre de l'instruction, sans qu'aucun décret d'extradition n'ait été pris au profit de l'Ukraine  que la durée de la privation de liberté de M. 
X...
dans le cadre de la présente procédure d'extradition a, de ce seul fait, excédé un délai raisonnable, en violation de l'article 5, § 1, de la Convention européenne des droits de l'homme  que la cassation interviendra sans renvoi et avec remise en liberté immédiate s'il n'est détenu pour autre cause  
" et aux motifs que M. 
X...
a souligné que la durée excessive de la détention dont il fait l'objet, sans limite dans le temps, porte atteinte au maintien des liens familiaux et au respect de la vie privée  qu'il faut rappeler que l'article 8 de la Convention européenne consacre le droit au respect de la vie privée et familiale, du domicile et de la correspondance  que l'ingérence de l'autorité publique est admise si elle est prévue par la loi et constitue, dans une société démocratique, une mesure nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui  qu'il n'est pas contestable que la vie familiale de M. 
X...
, même si celui-ci ne vivait pas avec son épouse et ses quatre enfants, vivant en Italie et en Suisse, est touchée par la mesure de détention dont il fait l'objet  que, cependant, cette situation est prévue par la loi s'agissant d'une détention ordonnée en exécution d'une demande d'extradition visant à en assurer l'effectivité  que bénéficiant de permis de visite avec sa famille et même de la possibilité de bénéficier de parloir à durée exceptionnelle avec les membres de sa famille, M. 
X...
ne peut donc invoquer une atteinte disproportionnée ainsi causée à sa vie familiale en raison d'une détention légale et régulière  
" 4°) alors qu'en se bornant à écarter une atteinte disproportionnée à la vie familiale de M. 
X...
eu égard au caractère légal et régulier de la mesure de détention dont il est l'objet et en s'abstenant, bien qu'elle y fût invitée, de tout contrôle de proportionnalité de la mesure eu égard à sa durée, qui n'est pas même évoquée, la chambre de l'instruction n'a pas légalement justifié sa décision "  
Vu l'article 5, § 1, f, de la Convention européenne des droits de l'homme  
Attendu qu'il résulte de cette disposition conventionnelle que si le déroulement d'une procédure d'extradition justifie une privation de liberté, c'est à la condition que cette procédure soit menée avec la diligence requise  
Attendu qu'il résulte de l'arrêt attaqué et des pièces de la procédure que M. 
X...
a été placé sous écrou extraditionnel respectivement le 1er août 2013 dans le cadre d'une demande d'extradition présentée par le gouvernement ukrainien puis le 5 novembre 2013 à la suite d'une demande formée par le gouvernement russe  que, par arrêts du 24 octobre 2014, la chambre de l'instruction de la cour d'appel de Lyon, statuant sur renvoi après cassation, a donné un avis favorable assorti de réserves à chacune de ces demandes, accordant une priorité de la remise aux autorités russes  que les pourvois formés par M. 
X...
ont été rejetés par arrêts de la Cour de cassation du 4 mars 2015  qu'un décret du 17 septembre suivant a fait droit à la seule demande d'extradition présentée par le gouvernement russe, M. 
X...
ayant ensuite exercé devant le Conseil d'Etat un recours en cours d'instruction  
Attendu que, pour rejeter la demande de mise en liberté formée par l'intéressé, qui invoquait la durée excessive de sa privation de liberté, la chambre de l'instruction, après avoir analysé la chronologie ininterrompue des différentes décisions rendues dont certaines sur les recours formés par M. 
X...
, avoir rappelé que celui formé contre le décret d'extradition vers la Russie est toujours en cours d'instruction devant le Conseil d'Etat, avoir constaté que l'absence de décret concernant la demande des autorités ukrainiennes ne procède pas d'une carence dans le traitement de cette procédure toujours en cours mais résulte de la priorité accordée à l'exécution de la demande de la Russie, en déduit que les autorités françaises ont conduit sans retard les deux procédures d'extradition particulièrement complexes ainsi que le traitement des recours formés par la personne réclamée et que la durée de la privation de liberté n'a pas excédé le délai nécessaire pour atteindre le but visé à l'article 5, § 1, f, de la Convention européenne des droits de l'homme  
Mais attendu qu'en prononçant ainsi alors que, si les diligences ont été accomplies sans retard dans la procédure d'extradition conduite à la demande de la Russie, celle concernant la demande de l'Ukraine est interrompue depuis l'arrêt de la Cour de cassation du 4 mars 2015 et le délai dans lequel elle pourra être éventuellement reprise se trouve indéterminé, la chambre de l'instruction, en fondant le rejet de la demande de mise en liberté sur les seules diligences accomplies dans une procédure d'extradition distincte, a méconnu le sens et la portée de la disposition conventionnelle susvisée  
D'où il suit que la cassation est encourue  qu'elle aura lieu sans renvoi, la Cour de cassation étant en mesure d'appliquer directement la règle de droit et de mettre fin au litige, ainsi que le permet l'article L. 411-3 du code de l'organisation judiciaire  
Par ces motifs et sans qu'il y ait lieu d'examiner le premier moyen de cassation, 
CASSE et ANNULE, en toutes ses dispositions, l'arrêt susvisé de la chambre de l'instruction de la cour d'appel de Lyon, en date du 5 juillet 2016  
DIT que la privation de liberté de M. 
X...
cesse d'être justifiée dans la procédure d'extradition suivie sur la demande des autorités ukrainiennes  
ORDONNE la mise en liberté de M. 
X...
, s'il n'est détenu pour autre cause  
DIT n'y avoir lieu à renvoi  
ORDONNE l'impression du présent arrêt, sa transcription sur les registres du greffe de la chambre de l'instruction de la cour d'appel de Lyon et sa mention en marge ou à la suite de l'arrêt annulé  
Ainsi jugé et prononcé par la Cour de cassation, chambre criminelle, en son audience publique, les jour, mois et an que dessus  
Etaient présents aux débats et au délibéré  M. Guérin, président, Mme Caron, conseiller rapporteur, M. Castel, Mme Caron, MM. Moreau, Stephan, conseillers de la chambre, M. Laurent, Mme Carbonaro, M. Beghin, conseillers référendaires  
Avocat général  M. Mondon  
Greffier de chambre  Mme Zita  
En foi de quoi le présent arrêt a été signé par le président, le rapporteur et le greffier de chambre.
