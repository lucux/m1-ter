#!/usr/bin/env python3
#-*- coding: utf-8 -*-
import os,sys
import re
filex=sys.argv[1]
okPrint=False #POUR TESTER METTRE A TRUE
def trim(d):
        return d.strip(' \t\n\r')
def printo(f,dec=1):
    global okPrint
    if okPrint:
        sd="\t"*dec
        print(str(sd)+str(f))
def giveMeTabu(file): #RECUPERER LE PREMIER PARAMETRE DANS LA LISTE DE COMMANDE BASH
    tab=[] #TABLEAU
    curr=[] #

    #l'article l. 761-1 du code de justice administrative.
    article=re.compile('^(l\')?article(s)?$') #EXPRESSION POUR SAVOIR QUAND "ARTICLE" PASSE AVEC DIFFERETN FORMAT
    stop=re.compile('^(\:)|(\;)|([^ \.\,]{2,}\.)|(<(FIN|fin)>)|([^ \.\,]{2,}\,)$') # STOPWORDS #([^ \.\,]{2,}\,)
    stopA=re.compile('^([^ ]{2,}\.)|([^ ]{2,}\,)$') #POUR SAVOIR QD C'EST UN POINT FINAL
    ok=1 #VARIABLE DE CONTROLE
    isRef=re.compile("^[-0-9]{1,},?$")
    finsS="^([^ \.\,]{2,})[\.\,].*$"
    fins=re.compile('^<(FIN|fin)>$')
    fir=0
    with open(file,'r') as f: #BOUCLE POUR LIRE LE FICHIER
        for line in f: #BOUCLE CHAQUE LINE 
            for word in line.split(): #BOUCLE CHAQUE MOT DE CHAQUE LIGNES
            # L'article 444 et l'article 33 du code XXXX
                # printo(word)
                # printo(ok)
                if article.match(word)  :
                    if ok==2:
                        tab.append(curr)
                    fir=0
                    ok=0 # tant que tu n'as pas de stopWords tu t'en fou de voir "article"
                    curr=[] #LE TABLEAU POUR CETTE ARTICLE
                else:
                    if ok!=1:
                        fir+=1
                        if word=="du" and fir==1:
                            continue
                        # printo(word)
                        # printo(stop.match(word))
                        # printo("true" if isRef.match(word) else "false")
                        if stop.match(word) and not isRef.match(word): #check if stopWord
                            if stopA.match(word): #check if stopWord point final
                                s=re.search(finsS,word)
                                #printo(word)
                                #printo(s.group(1))
                                curr.append(s.group(1)) #on enleve le point final
                            tab.append(curr)
                            ok=1 #maintenant on peux reperer le mot article
                        else:
                            curr.append(word)
                            ok=2
    # print tab
    # sys.exit(1)
    #fonction qui te renvoie le texte du fichier strs
    def read_text(strs, encoding=None, errors=None):
        """
        Open the file in text mode, read it, and close the file.
        """
        with open(strs, 'r') as content_file:
            return content_file.read()
    # def getWords():
    #     pass
    #liste des mots "code de ....."
    wordsCode=read_text("codes.csv").split("\n") #tableau des codes
    printo("TAB-----")
    printo(tab)
    # exit(1)
    # # du code

    # ([1-9]+)|(([1-9]+-[1-9]+)+)|([1-9]+-[1-9]+)+(,| et) [1-9]+-[1-9]+
    mot1="du" # reperer format
    mot2="code" # reperer format 
    tabo=[] #on s'en fou 
    ok2=False  #variable de controle
    kk=0  #variable de controle
    Laststring="PB" #code d'avant
    string="code" #code .....
    # code=re.compile('^(l\')article(s)?$')
    # from collections import defaultdict

    articles = {} #{clef=>values} clef = article + code de .... Values : nb d'occurences

    for m,i in enumerate(tab): #On boucle sur le 1er tableau
        curr=wordsCode 
        kk=0
        string="code"  #le code ....
        #Laststring="PB" # le dernier code que l'on ai trouvé
        string2="" #reference de l'article
        ok2=False
        # if m==5:
        #     break
        for num,j in enumerate(i): #mot de chaque lignes
            #print "DEBUG: "+j+" string: "+string+" string2: "+string2+" ok2: "+str(ok2)+" kk: "+str(kk)+" curr: "
            #print(len(curr))
            # if len(curr)<5:
            #    print(curr)
            if ok2: 
                if len(curr)>1:
                    js=j.replace(",","").replace(" ","") #on enleve espace et virgule
                    if stopA.match(js):
                        js=js.replace(".","")#on enleve le pt final
                    string+=" "+js 
                    #"code de"
                    curr=[x for x in curr if string in x]
                if len(curr)==0:
                    sentence = string2
                    # pattern = re.compile(r'')
                    # sentence = re.sub(pattern, '', string2)
                    if Laststring=="PB":
                            continue
                    tabo.append([sentence,Laststring])
                    sf=sentence+" "+Laststring
                    if sf not in articles:
                        articles[sf]=1
                    else:
                        articles[sf]+=1

                    break
                elif len(curr)==1:
                    sentence = string2
                    # pattern = re.compile(r'') #enlever les espaces dans la references
                    # sentence = re.sub(pattern, '', string2) #enlever les espaces dans la references
                    string=curr[0] #tu recuperes le 1ere element dans la liste de codes
                    tabo.append([sentence,string])
                    Laststring=string 
                    sf=sentence+" | "+string #tu mets ensemble ref et code
                    if sf not in articles: #est ce que j'ai deja ajouter dans le tableau articles sf ?
                        articles[sf]=1 
                    else:
                        articles[sf]+=1
                    # print(sf)
                    # print("sfok")
                    break
    #PB  SI PAS FORMAT DU CODE         
            else:
                if kk==1:
                    if j==mot2: #MOT == "code"
                        ok2=True # on a la ref de l'article dans string2 et on sait qu'on va savoir mtn de quelle code on parle
                        continue
                    else:
                        kk=0 
                        sentence = string2
                        # pattern = re.compile(r'')
                        # sentence = re.sub(pattern, '', string2)
                        # print("Adummcode: "+sentence)
                        # print("ALaststring: "+Laststring)
                        if Laststring=="PB":
                            continue
                        # print("dummcode: "+sentence)
                        tabo.append([sentence,Laststring])
                        sf=sentence+" | "+Laststring
                        if sf not in articles:
                            articles[sf]=1
                        else:
                            articles[sf]+=1
                        break
                # if j=="et": #est ce que le mot et egal à "et"
                #     string2="" #poubelles 
                #     continue
                if j==mot1: # mot == "du"
                    kk+=1
                    continue
                else:
                    string2+=" "+j
            if string2!="" and num==len(i)-1:
                sentence = string2
                # pattern = re.compile(r'')
                # sentence = re.sub(pattern, '', string2)
                # print("AAdummcode: "+sentence)
                # print("AALaststring: "+Laststring)
                if Laststring=="PB":
                    continue
                # print("dummcode: "+sentence)
                tabo.append([sentence,Laststring])
                sf=sentence+" | "+Laststring
                if sf not in articles:
                    articles[sf]=1
                else:
                    articles[sf]+=1

    # } >/dev/null
    date=re.compile("[0-9]{4}")  
    wordsEuro=read_text("europ").split("\n") #tableau des codes
    #wordsCode
    wordsConv=read_text("conventionsUN").split("\n")
    tabu=[]
    conjonction=re.compile('d(u|a|e(s)?)')
    last=""
    printo("T2-----")
    for m,i in enumerate(tab): #On boucle sur le 1er tableau
        okA=False
        step=0
        wordl=[]
        codn=[]
        curr=wordsCode+wordsEuro+wordsConv 
        string=""
        loiD=False
        dum=False
        otherTab=[]
        finish=False
        okAAll=True
        for num,j in enumerate(i):
            if okA:
                okAAll=False
                string+=j+" "
                printo(string)
                curr=[x for x in curr if trim(string) in x]
                if num+1==len(i):
                    print(string)
                    # string=re.escape(string)
                    # print(trim(string))
                    currFIN=re.compile(r"^.*"+trim(string)+"$") 
                    # print(r""+trim(string)+"")
                    # print(curr)
                    curr=[trim(x) for x in curr if currFIN.match(trim(x))]
                    # print(curr)
                printo(len(curr))
                if len(curr)==0:
                    curr=wordsCode+wordsEuro+wordsConv
                    printo(string)
                    if trim(string)=="la loi":
                        loiD=True
                    if trim(string)=="decret":
                        string="la decret "
                        loiD=True
                    if trim(string)=="loi":
                        string="la loi "
                        loiD=True
                    if trim(string)=="loi du":
                        string="la loi du "
                        loiD=True
                    fsf=trim(string)
                    if fsf.split(" ")[-1]=="ordonnance":
                        string="   ordonnance "
                        dum=True
                        break
                    if trim(string)=="l'ordonnance":
                        string="de ordonnance "
                        loiD=True
                    if trim(string)=="meme code" or trim(string)=="dudit code" or trim(string)=="ce code":
                        dum=True
                    if loiD:
                        if date.match(j):
                            printo("<<date>>")
                            last=trim(string)[3:]
                            tabu=addToTabu(tabu," ".join(codn),last)
                            loiD=False
                            codn=[]
                            codn=[]
                            okA=False
                            curr=wordsCode+wordsEuro+wordsConv 
                            string=""
                            break
                    elif not loiD and not dum:
                        string=j+" "
                elif len(curr)==1:
                    if curr[0].split(" ")[-1]==trim(string).split(" ")[-1]:
                        last=curr[0]
                       
                        printo("<<EXACT>>")
                        printo(codn)
                        if len(trim(" ".join(codn)))!=0:
                            tabu=addToTabu(tabu," ".join(codn),curr[0])
                        finish=True
                        okA=False
                        codn=[]
                        codn=[]
                        curr=wordsCode+wordsEuro+wordsConv 
                        string=""
                        break
            else:
                if conjonction.match(j):
                    okA=True
                else:
                    codn.append(j)
        if loiD:
            if trim(string)[3:]=="loi":
                string="la "+last
            printo("<<loiD>>")
            last=trim(string)[3:]
            tabu=addToTabu(tabu," ".join(codn),last)
            loiD=False
        ksf=trim(string).split(" ")
        if ksf[-1]=="loi":
            dum=True
        if len(i)==1:
            dum=True
        if not okA:
            # print("sFFFF")
            # print(" ".join(codn))
            # print("----sFFFF")
            # print(len(codn))
            dum=True
        if dum:
            if len(last)==0 or len(codn)==0:
                dum=False
                continue
            printo("<<duM>>")
            tabu=addToTabu(tabu,removeP(" ".join(codn)),last)
            dum=False
            
        # tabu.append([" ".join(codn)," ".join(wordl)])



    return tabu
def oneSpace(co):
    return ' '.join(co.split())
def hasNumbers(inputString):
    return bool(re.search(r'\d', inputString))
regexEtO=r"((?:[a-z])?\.?(?: )?[-0-9 ]+|[a-z]|bis)"
regexEtOO=r"^"+regexEtO+"$"
regex0=r"(?:([^(?:ou|et|a|,)]+)(?: (?:ou|et|a|\,))?)+"
regexEt=r"^(?:"+regexEtO+"(?: (?:et|a|ou) )"+regexEtO+"+)$"
motsPa=[" et suivants"," precite"," cidessus", "alinea","precitee"]
# print(regexEtOO)
def wordInCo(co):
    global motsPa
    for i in motsPa:
        if i in co:
            co=co.replace(i,"")
    # if co[-1]==",":
    #     co=co[:-1]
    return co
def virguleS(co):
    co=co.replace(","," , ")
    return oneSpace(co)
# xml_ok4/CETATEXT000034069164.xml
def addToTabu(tabu,co,la):
    global regexEt,regexEtO,regex0
    co=virguleS(removeP(wordInCo(co)))
    # print(type(co))
    # print(co)
    sfj=re.findall(regex0,co)
    # print("jjf")
    # print(sfj)
    # print("/jjf/")
    # # sfj=sfj[0] if len(sfj)>0 else sfj
    # print("jj")
    # print(sfj)
    # print("/jj/")
    # if isinstance(sfj,str):
    #     sfj=[sfj]
    if len(sfj)==1:
        # print(regexEtOO,co)
        if not hasNumbers(co):
            return tabu
        sd=re.findall(regexEtO,wordInCo(co))
        # print(sd)
        if len(sd)!=0:
            co=sd[0]
        # else:

        tabu.append([removeP(trim(co),True),removeP(trim(la),True)])
    else:

        for i in range(len(sfj)):
            tabu=addToTabu(tabu,trim(sfj[i]),la)
        # exit(1)
    # print(tabu)
    return tabu
remP=["-",".","°",")","(","º","\"","\'"]
def removeP(co,withV=False):
    global remP
    s=remP
    if withV:
        s+=","
    for i in s:
        if i in co:
            co=co.replace(i,"")
    return co
if okPrint:
    printo("TABU--------------------------------",0)
    op=giveMeTabu(filex)
    printo("RESULT------------------------------",0)
    for i in op:
        print(i[0]+" "+i[1])
else:
# print articles
    tabop={}
    doc={}
    docI={}

    for subdir, dirs, files in os.walk(filex):
        for file in files:
            filej=os.path.join(subdir, file)
            docH=hash(filej)
            docI[docH]=filej
            doc[filej]=docH
            # print(filej)
            tabop[docH]=giveMeTabu(filej)

    article={}
    occur={}
    articleI={}
    tu={}
    for p,j in tabop.items(): #{ hashDoc : [[3,code civil],[4 partnont]]}
        tabk={}
        for k in j:
            kk=k[0]
            kk=removeP(kk)
            code=kk+" "+k[1]
            code=re.sub(' +',' ',code)
            codeh=hash(code)
            article[code]=codeh
            articleI[codeh]=code
            if codeh not in occur:
                occur[codeh]=0
            occur[codeh]+=1
            if codeh not in tabk:
                tabk[codeh]=0
            tabk[codeh]+=1
        if len(tabk)>0:
            tu[p]=tabk



    import pickle
    pickle.dump(doc,open("doc.data","wb"),protocol=0)
    pickle.dump(docI,open("docI.data","wb"),protocol=0)
    pickle.dump(article,open("article.data","wb"),protocol=0)
    pickle.dump(tu,open("dtm.data","wb"),protocol=0)
    pickle.dump(occur,open("occur.data","wb"),protocol=0)
    pickle.dump(articleI,open("articleI.data","wb"),protocol=0)
#print(giveMeTabu(file))