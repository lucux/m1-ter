<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_JUDI>
<META>
<META_COMMUN>
<ID>JURITEXT000032412371</ID>
<ANCIEN_ID/>
<ORIGINE>JURI</ORIGINE>
<URL>texte/juri/judi/JURI/TEXT/00/00/32/41/23/JURITEXT000032412371.xml</URL>
<NATURE>ARRET</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Cour de cassation, civile, Chambre civile 1, 6 avril 2016, 15-10.552, Publié au bulletin</TITRE>
<DATE_DEC>2016-04-06</DATE_DEC>
<JURIDICTION>Cour de cassation</JURIDICTION>
<NUMERO>11600341</NUMERO>
<SOLUTION>Cassation partielle</SOLUTION>
</META_JURI>
<META_JURI_JUDI>
<NUMEROS_AFFAIRES>
<NUMERO_AFFAIRE>15-10552</NUMERO_AFFAIRE>
</NUMEROS_AFFAIRES>
<PUBLI_BULL publie="oui"/>
<FORMATION>CHAMBRE_CIVILE_1</FORMATION>

<FORM_DEC_ATT>Cour d'appel de Bastia</FORM_DEC_ATT>
<DATE_DEC_ATT>2014-11-12</DATE_DEC_ATT>
<SIEGE_APPEL/>
<JURI_PREM/>
<LIEU_PREM/>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme Batut</PRESIDENT>
<AVOCAT_GL>M. Drouet</AVOCAT_GL>
<AVOCATS>SCP Didier et Pinet, SCP Spinosi et Sureau</AVOCATS>
<RAPPORTEUR>Mme Canas</RAPPORTEUR>
<ECLI>ECLI:FR:CCASS:2016:C100341</ECLI>
</META_JURI_JUDI>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<p>LA COUR DE CASSATION, PREMIÈRE CHAMBRE CIVILE, a rendu l'arrêt suivant : </p>
<p>
<br clear="none"/>Attendu, selon l'arrêt attaqué, que la commune d'Ajaccio a confié à la société X... et fils (la société), par une délégation de service public, la construction et l'exploitation d'un crématorium situé au lieudit du Vazzio ; que, soutenant que le tract diffusé par un collectif dénommé « Collectif contre le crématorium au Vazzio » (le collectif), ainsi que la pétition que celui-ci avait mise en ligne sur Internet, contenaient des propos diffamatoires à leur égard, la société, MM. Y... et Marc-Xavier X... et Mmes Françoise, Valérie et Elodie X... (les consorts X...) ont assigné les membres de ce collectif et la société Groupe Nextone Media Limited, hébergeur du site en cause, aux fins d'obtenir réparation de leur préjudice ; </p>
<p>Sur le moyen tiré de la nullité de l'assignation, relevé d'office, après avis donné aux parties conformément à l'article 1015 du code de procédure civile : </p>
<p>Vu l'article 53 de la loi du 29 juillet 1881 sur la liberté de la presse ; </p>
<p>Attendu qu'en vertu de ce texte, l'assignation doit, à peine de nullité, préciser et qualifier le fait incriminé, et indiquer le texte de loi applicable ; </p>
<p>Attendu que, selon une jurisprudence constante, la chambre criminelle de la Cour de cassation décide qu'elle a le devoir de vérifier, d'office, si la citation délivrée est conforme au texte susvisé et, notamment, qu'elle mentionne le texte qui édicte la peine sanctionnant l'infraction poursuivie ; que la première chambre civile de la Cour de cassation a cependant jugé que la seule omission, dans l'assignation, de la mention de la sanction pénale encourue, que la juridiction civile ne peut prononcer, n'était pas de nature à en affecter la validité (1re Civ., 24 septembre 2009, pourvoi n° 08-17. 315, Bull. n° 180) ; que, toutefois, par arrêt du 15 décembre 2013 (pourvoi n° 11-14. 637, Bull. n° 1), l'Assemblée plénière de la Cour de cassation, saisie de la question de la validité d'une assignation retenant pour le même fait la double qualification d'injure et de diffamation, a affirmé que l'article 53 de la loi du 29 juillet 1881 devait recevoir application devant la juridiction civile ; que cette décision, qui consacre l'unicité du procès de presse, conduit à une modification de la jurisprudence précitée, justifiée par la nécessité d'unifier les règles relatives au contenu de l'assignation en matière d'infractions de presse, que l'action soit engagée devant la juridiction civile ou la juridiction pénale ; </p>
<p>Attendu qu'en l'espèce, les assignations délivrées à la requête de la société et des consorts X... visent l'article 29 de la loi du 29 juillet 1881, mais non l'article 32 de la même loi ; que, dès lors, à défaut de mention du texte édictant la peine applicable aux faits de diffamation allégués, ces assignations encourent la nullité ; </p>
<p>Attendu, cependant, que, si la jurisprudence nouvelle s'applique de plein droit à tout ce qui a été fait sur la base et sur la foi de la jurisprudence ancienne, la mise en oeuvre de ce principe peut affecter irrémédiablement la situation des parties ayant agi de bonne foi, en se conformant à l'état du droit applicable à la date de leur action, de sorte que le juge doit procéder à une évaluation des inconvénients justifiant qu'il soit fait exception au principe de la rétroactivité de la jurisprudence et rechercher, au cas par cas, s'il existe, entre les avantages qui y sont attachés et ses inconvénients, une disproportion manifeste ; que les assignations en cause, dont les énonciations étaient conformes à la jurisprudence de la première chambre civile, ont été délivrées à une date à laquelle la société et les consorts X... ne pouvaient ni connaître ni prévoir l'obligation nouvelle de mentionner le texte édictant la peine encourue ; que, dès lors, l'application immédiate, à l'occasion d'un revirement de jurisprudence, de cette règle de procédure dans l'instance en cours aboutirait à priver ces derniers d'un procès équitable, au sens de l'article 6, § 1, de la Convention de sauvegarde des droits de l'homme et des libertés fondamentales, en leur interdisant l'accès au juge ; qu'il n'y a donc pas lieu d'annuler les assignations ; </p>
<p>Sur le moyen unique, pris en sa deuxième branche : </p>
<p>Vu l'article 4 du code de procédure civile ; </p>
<p>Attendu que, pour rejeter les demandes formées par la société et les consorts X..., l'arrêt énonce que, selon leurs conclusions, les faits de diffamation dénoncés tiendraient au projet de création d'une usine à brûler des corps humains, générant une pollution aussi dangereuse que des déchets nucléaires, dans un site habité, contrairement à l'usage répandu sur le territoire national ; </p>
<p>Qu'en statuant ainsi, alors que la société et les consorts X... incriminaient également l'emploi, dans le texte de la pétition, des termes « usine équipée de fours crématoires » et soutenaient que ces termes faisaient référence aux fours crématoires des camps d'extermination nazis, la cour d'appel a méconnu l'objet du litige et violé le texte susvisé ; </p>
<p>PAR CES MOTIFS, et sans qu'il y ait lieu de statuer sur les autres branches du moyen : </p>
<p>CASSE ET ANNULE, mais seulement en ce qu'il rejette les demandes de la société X... et fils, de MM. 
Y...
et Marc-Xavier X... et de Mmes Françoise, Valérie et Elodie X..., l'arrêt rendu le 12 novembre 2014, entre les parties, par la cour d'appel de Bastia ; remet, en conséquence, sur ce point, la cause et les parties dans l'état où elles se trouvaient avant ledit arrêt et, pour être fait droit, les renvoie devant la cour d'appel de Lyon ; </p>
<p>Condamne les défendeurs au pourvoi aux dépens ; </p>
<p>Vu l'article 700 du code de procédure civile, rejette les demandes ; </p>
<p>Dit que sur les diligences du procureur général près la Cour de cassation, le présent arrêt sera transmis pour être transcrit en marge ou à la suite de l'arrêt partiellement cassé ; </p>
<p>Ainsi fait et jugé par la Cour de cassation, première chambre civile, et prononcé par le président en son audience publique du six avril deux mille seize. </p>
<br/>
<p>MOYEN ANNEXE au présent arrêt </p>
<p>Moyen produit par la SCP Didier et Pinet, avocat aux Conseils, pour les consorts X... et la société X... et fils. </p>
<p>Il est fait grief à l'arrêt attaqué d'AVOIR dit que l'action en diffamation n'était pas fondée et d'avoir débouté la SAS X...et Fils, M. Y...
X..., Mme Françoise X..., Mme Valérie X..., M. Marc Xavier X... et Mme Elodie X... de leurs demandes ; </p>
<p>AUX MOTIFS QUE dans leurs dernières conclusions déposées le 5 mai 2014 auxquelles il convient de se référer pour un exposé complet de ses prétentions et parties, les appelants exposent que la famille X... a développé depuis 1972 une entreprise de pompes funèbres exercée sous forme de SARL à compter de 1995 puis de SAS en 2005, laquelle a bénéficié de la ville d'Ajaccio d'une délégation de service public du 14 décembre 2011 ayant pour objet la construction et l'exploitation d'un crématorium sur la commune d'Ajaccio ; qu'un arrêté de permis de construire a été accordé le 5 juillet 2012 ; qu'ils expliquent que le 27 juillet 2012 a été mis en ligne une pétition émanant d'un « collectif contre le crématorium au Vazzio » et qu'un tract a également été distribué sur la commune d'Ajaccio qu'ils estiment être diffamatoires à leur égard en application de l'article 29 de la loi du 29 juillet 1881 ; que pour répondre à l'argumentation des intimés, ils réfutent la nullité de l'assignation au regard des prescriptions de l'article 53 de la loi du 29 juillet 1881 et la prescription trimestrielle retenue par le premier juge ; que sur le fond, les appelants soutiennent que les faits de diffamation ressortent des écrits tant sur le tract que sur la pétition en ligne sur Internet contre la réalisation d'un crématorium au Vazzio ; qu'ils soutiennent que la famille X... est parfaitement identifiable et qu'il est spécifiquement précisé que va être créée une usine à brûler des corps humains, générant une pollution aussi dangereuse que des déchets nucléaires, dans un site habité contrairement à l'usage répandu sur le territoire national et dans les conditions de la vidéo en ligne telles que décrites par Me Marie-Pierre F., huissier de justice, le 17 septembre 2012 ; que l'ensemble de ces faits est constitutif d'une atteinte à l'honneur et à la considération de la SAS X...&amp; Fils qui si elle n'est pas explicitement nommée, est parfaitement identifiable au regard à la fois de l'ensemble des articles de presse relatifs à la création du crématorium, à la publicité légale et réglementaire de l'ensemble des actes administratifs liés à cette construction, que ce soit la délégation de service public, l'enquête publique, l'arrêté de permis de construire ou la tenue du Conseil Départemental de l'Environnement des Risques Sanitaires et Technologique ou encore l'arrêté préfectoral d'autorisation de création du crématorium ; (…) qu'il convient de constater que les intimés ne contestent pas être les auteurs du tract et de la pétition en ligne renvoyant à un site Web sur l'incinération ; (…) que, sur le fond, le tract et la pétition ont fait l'objet d'un constat d'huissier auquel il conviendra de se reporter pour en connaître le contenu dans le détail d'ailleurs repris dans les conclusions des appelants en leur entier ; qu'il convient de noter que ni le tract ni la pétition ne cite la SAS X...&amp; Fils ni aucun des membres de la famille X... et qu'elles ne mettent en cause que la création d'un crématorium sur un site que les auteurs jugent inopportun ; que le tract aussi bien que la pétition ont pour but de s'opposer à la création d'un crématorium dans un lieu non isolé ; qu'il appartient aux parties poursuivantes de préciser les passages de l'article qu'elles estiment diffamatoires au regard des dispositions de la loi du 29 juillet 1881 ; que si l'on s'en tient aux conclusions ci-dessus rappelées, les faits de diffamation dénoncés tiendraient aux faits que va être créée une usine à brûler des corps humains, générant une pollution aussi dangereuse que des déchets nucléaires, dans un site habité contrairement à l'usage répandu sur le territoire national et dans les conditions de la vidéo en ligne ; que les auteurs du tract et de la pétition critiquent la création d'un crématorium, dont, pour eux, l'activité est nocive, critiquent également l'absence de concertation avec les riverains lors des décisions administratives prises en vue de cette création, mais ne mettent nullement en cause l'honneur et la considération tant de la SAS susnommée que les membres de la famille X... ; qu'en conséquence, l'action n'est pas fondée et les appelants seront déboutés de leurs demandes ; </p>
<p>1°) ALORS QUE s'il n'expose pas succinctement les prétentions respectives des parties et leurs moyens, le juge, qui ne peut statuer que sur les dernières conclusions déposées, doit viser celles-ci avec l'indication de leur date ; que l'arrêt qui vise des conclusions (du 5 mai 2014) antérieures aux dernières déposées (le 21 juillet 2014) et n'expose, ni ne rappelle dans la motivation, que les exposants faisaient valoir que la diffamation était notamment constituée l'allégation de ce que les exposants allaient mettre en oeuvre des procédés assimilables aux fours crématoires des camps d'extermination nazis et maltraitant les dépouilles des défunts, viole les articles 455 et 954 du code de procédure civile ; </p>
<p>Subsidiairement </p>
<p>2°) ALORS QU'en retenant qu'à s'en tenir aux conclusions des exposants, les faits de diffamation dénoncés tiendraient aux faits que va être créée une usine à brûler des corps humains, générant une pollution aussi dangereuse que des déchets nucléaires, dans un site habité contrairement à l'usage répandu sur le territoire national et dans les conditions de la vidéo en ligne, quand les exposés avaient fait valoir que la diffamation était également constituée par l'allégation de ce que les exposants allaient mettre en oeuvre des procédés assimilables aux fours crématoires des camps d'extermination nazis et maltraitant les dépouilles des défunts, la cour d'appel a méconnu les prétentions des exposants en violation de l'article 4 du code de procédure civile ; </p>
<p>3°) ALORS QU'en retenant que les auteurs du tract et de la pétition critiquaient la création d'un crématorium, dont, pour eux, l'activité était nocive, critiquaient également l'absence de concertation avec les riverains lors des décisions administratives prises en vue de cette création, mais ne mettaient nullement en cause l'honneur et la considération tant de la SAS susnommée que les membres de la famille X..., sans répondre au moyen pris de ce que les intéressés imputaient aux exposants le projet de mettre en oeuvre des procédés assimilables aux fours crématoires des camps d'extermination nazis, et maltraitant les dépouilles des défunts, la cour d'appel a violé l'article 455 du code de procédure civile ; </p>
<p>4°) ALORS QU'en se fondant, pour écarter la diffamation, sur le fait que ni le tract ni la pétition ne citaient la SAS X...&amp; Fils ni aucun des membres de la famille X..., sans rechercher si, ainsi que le faisaient valoir les exposants, ils n'étaient pas parfaitement identifiables au regard de l'ensemble des articles de presse relatifs à la création du crématorium, à la publicité légale et réglementaire de l'ensemble des actes administratifs liés à cette construction et compte tenu de ce que seules la famille X... et la SAS X...&amp; Fils exercent une activité de funérarium dans la zone industrielle du Vazzio, la cour d'appel a privé sa décision de base légale au regard de l'article 29 de la loi du 29 juillet 1881 ; </p>
<p>5°) ALORS QUE porte atteinte à l'honneur et à la considération l'allégation mensongère de ce que l'activité d'une entreprise génère une pollution dangereuse pour la santé des vivants, entraînant le rejet de métaux lourds, de mercure et de dioxine cancérigènes et produisant des déchets assimilables aux déchets nucléaires ; qu'en décidant le contraire, la cour d'appel a violé l'article 29 de la loi du 29 juillet 1881 ; </p>
<p>6°) ALORS QU'en retenant que les auteurs du tract et de la pétition critiquaient la création d'un crématorium, dont, pour eux, l'activité était nocive ainsi que l'absence de concertation avec les riverains lors des décisions administratives prises en vue de cette création, mais ne mettaient pas en cause l'honneur et la considération tant de la SAS susnommée que les membres de la famille X..., sans rechercher si, ainsi que le soutenaient les exposants, cette mise en cause n'était pas constituée par l'affirmation que l'activité de crémation de l'exposante était réalisée dans des conditions n'assurant pas le respect dû à la dépouille des défunts, la cour d'appel a privé sa décision de base légale au regard de l'article 29 de la loi du 29 juillet 1881.</p>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="1" TYPE="PRINCIPAL">PRESSE - Procédure -  Action en justice -  Assignation -  Validité -  Conditions -  Indication du texte de loi applicable -  Défaut -  Sanction -  Nullité de l'assignation en son entier
</SCT>
<SCT ID="1" TYPE="REFERENCE">PRESSE -  Procédure -  Action en justice -  Assignation -  Validité -  Conditions -  Article 53 de la loi du 29 juillet 1881 CONVENTION EUROPEENNE DES DROITS DE L'HOMME -  Article 6, § 1 -  Equité -  Egalité des armes -  Violation -  Cas -  Application immédiate d'une règle jurisprudentielle nouvelle -  Applications diverses -  Règle de procédure imposant de faire mention, dans l'assignation, du texte édictant la peine applicable aux faits de diffamation allégués
</SCT>
<ANA ID="1">En vertu de l'article 53 de la loi du 29 juillet 1881 sur la liberté de la presse, l'assignation doit, à peine de nullité, préciser et qualifier le fait incriminé, et indiquer le texte de loi applicable.
Encourt, par suite, la nullité une assignation qui ne fait pas mention du texte édictant la peine applicable aux faits de diffamation allégués.
Il n'y a pas lieu, cependant, en l'espèce, d'annuler les assignations délivrées à la requête des demandeurs au pourvoi, dès lors que l'application immédiate, à l'occasion d'un revirement de jurisprudence, de cette règle de procédure dans l'instance en cours aboutirait à les priver d'un procès équitable, au sens de l'article 6, § 1, de la Convention de sauvegarde des droits de l'homme et des libertés fondamentales, en leur interdisant l'accès au juge</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>Sur les conditions de validité de l'assignation en matière de presse devant les juridictions civiles, à rapprocher :1re Civ., 4 février 2015, pourvoi n° 13-19.455, Bull. 2015, I, n° 26 (cassation sans renvoi), et les arrêts cités.Sur l'impossibilité d'appliquer immédiatement certains revirements de jurisprudence, dans le même sens que :Com., 26 octobre 2010, pourvoi n° 09-68.928, Bull. 2010, IV, n° 159 (rejet), et les arrêts cités


</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS>
<LIEN cidtexte="" datesignatexte="2014-11-12" id="JURITEXT000029770543" naturetexte="ARRET" nortexte="" num="" numtexte="" sens="source" typelien="SUITE_PROCEDURALE">Cour d'appel de Bastia, 12 novembre 2014, 13/00654</LIEN>
<LIEN cidtexte="" datesignatexte="" id="" naturetexte="" nortexte="" num="" numtexte="" sens="source" typelien="CITATION">article 53 de la loi du 29 juillet 1881 sur la liberté de la presse ;  article 6, § 1, de la Convention de sauvegarde des droits de l'homme et des libertés fondamentales</LIEN>
</LIENS>
</TEXTE_JURI_JUDI>
