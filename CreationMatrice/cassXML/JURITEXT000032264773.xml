<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_JUDI>
<META>
<META_COMMUN>
<ID>JURITEXT000032264773</ID>
<ANCIEN_ID/>
<ORIGINE>JURI</ORIGINE>
<URL>texte/juri/judi/JURI/TEXT/00/00/32/26/47/JURITEXT000032264773.xml</URL>
<NATURE>ARRET</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Cour de cassation, civile, Chambre civile 1, 16 mars 2016, 15-13.427, Publié au bulletin</TITRE>
<DATE_DEC>2016-03-16</DATE_DEC>
<JURIDICTION>Cour de cassation</JURIDICTION>
<NUMERO>11600229</NUMERO>
<SOLUTION>Rejet</SOLUTION>
</META_JURI>
<META_JURI_JUDI>
<NUMEROS_AFFAIRES>
<NUMERO_AFFAIRE>15-13427</NUMERO_AFFAIRE>
</NUMEROS_AFFAIRES>
<PUBLI_BULL publie="oui"/>
<FORMATION>CHAMBRE_CIVILE_1</FORMATION>

<FORM_DEC_ATT>Cour d'appel de Paris</FORM_DEC_ATT>
<DATE_DEC_ATT>2015-01-20</DATE_DEC_ATT>
<SIEGE_APPEL/>
<JURI_PREM/>
<LIEU_PREM/>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme Batut</PRESIDENT>
<AVOCAT_GL>Mme Valdès-Boulouque</AVOCAT_GL>
<AVOCATS>SCP Piwnica et Molinié, SCP Waquet, Farge et Hazan</AVOCATS>
<RAPPORTEUR>Mme Le Cotty</RAPPORTEUR>
<ECLI>ECLI:FR:CCASS:2016:C100229</ECLI>
</META_JURI_JUDI>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<p>LA COUR DE CASSATION, PREMIÈRE CHAMBRE CIVILE, a rendu l'arrêt suivant : </p>
<p>Attendu, selon l'arrêt attaqué (Paris, 20 janvier 2015), que A...
X... a été inscrit sur les registres de l'état civil comme né le 10 février 2007 de Mme X... ; que, par acte du 9 janvier 2013, cette dernière a assigné M. Y... devant un tribunal afin de voir établir sa paternité vis-à-vis de l'enfant ; </p>
<p>Sur le premier moyen : </p>
<p>Attendu que M. Y... fait grief à l'arrêt de dire qu'il est le père de A...
X... alors, selon le moyen : </p>
<p>1°/ qu'une présomption de filiation n'est attachée par la loi à une procréation médicalement assistée que dans la mesure où elle a été pratiquée dans le cadre et en respectant les conditions fixées par la loi ; que la loi réserve l'assistance médicale à la procréation aux cas d'infertilité du couple ou pour éviter la transmission à l'enfant d'une maladie d'une particulière gravité ; qu'en ne recherchant pas si Mme X..., qui indiquait avoir interrompu volontairement, à la demande de M. Y..., une précédente grossesse, était éligible à la procréation médicalement assistée et pouvait se prévaloir de la présomption de filiation prévue par la loi, la cour d'appel a privé sa décision de base légale au regard des articles L. 2141-1 et suivants du code de la santé publique et 327 et suivants du code civil ; </p>
<p>2°/ que l'homme et la femme qui ont recours à une procréation médicalement assistée doivent former un couple, être animés d'un projet parental et vivre ensemble ; qu'en retenant que même à supposer que M. Y... et Mme X... n'aient pas eu de projet parental et se soient ainsi placés hors du cadre fixé par la loi, la présomption de filiation résultant du recours à une procréation médicalement assistée n'en trouvait pas moins à s'appliquer, la cour d'appel a violé les articles L. 2141-1 et suivants du code de la santé publique et 327 et suivants du code civil ; </p>
<p>3°/ qu'à défaut d'être établie par la présomption résultant du recours régulier à une procréation médicalement assistée, la filiation, hors mariage, s'établit et se conteste par tous moyens ; que la cour d'appel a refusé d'envisager la possibilité d'une conception naturelle évoquée par le docteur Z..., la considérant comme « inopérante » compte tenu de la stérilité de Mme X... ; qu'en s'abstenant de rechercher si Mme X..., qui indiquait avoir déjà été enceinte, ne pouvait pas avoir conçu A... de façon naturelle, de sorte que cette éventualité était opérante et devait être examinée, au besoin en ordonnant une expertise pour trancher la filiation de l'enfant, la cour d'appel a violé les articles 327 et suivants du code civil ; </p>
<p>Mais attendu que, contrairement aux énonciations du moyen, les juges du fond ne se sont pas fondés sur une présomption de filiation, mais ont retenu, à bon droit, que l'établissement judiciaire de la filiation à la suite d'une procréation médicalement assistée sans tiers donneur obéissait aux règles générales édictées par les articles 327 et suivants du code civil et qu'en application des dispositions du second alinéa de l'article 310-3 du même code, la preuve de la paternité pouvait être apportée par tous moyens ; </p>
<p>Et attendu qu'ayant relevé, par motifs propres et adoptés, que M. Y... et Mme X... avaient entretenu une relation sentimentale à compter de l'année 1997, qu'ils avaient signé un « consentement en vue d'insémination artificielle du couple », que, le 20 avril 2006, M. Y... avait donné son accord pour la congélation de son sperme pour permettre à Mme X... de recourir à la procréation médicalement assistée et que les éléments du dossier établissaient le lien existant entre les gamètes données par M. Y..., l'insémination artificielle de Mme X..., sa grossesse, l'accouchement et la naissance de l'enfant, la cour d'appel, qui a constaté que M. Y... ne versait pas le moindre commencement de preuve des prétendues relations intimes de Mme X... avec d'autres hommes et que celle-ci était suivie pour infertilité, en a déduit, procédant à la recherche prétendument omise, que M. Y... était le père de l'enfant ; </p>
<p>D'où il suit que le moyen, inopérant en ses deux premières branches, n'est pas fondé pour le surplus ; </p>
<p>Sur le second moyen : </p>
<p>Attendu que M. Y... fait le même grief à l'arrêt alors, selon le moyen : </p>
<p>1°/ que le consentement donné à une procréation médicalement assistée interdit toute action aux fins d'établissement ou de contestation de la filiation à moins qu'il ne soit soutenu que l'enfant n'en est pas issu ; que M. Y... contestait que l'enfant soit issu de l'insémination litigieuse ; qu'en retenant qu'il était le père de l'enfant sans rechercher, s'il était issu de la procréation médicalement assistée, la cour d'appel a privé sa décision de base légale au regard de l'article 311-20 du code civil ; </p>
<p>2°/ que le consentement donné à une procréation médicalement assistée interdit toute action aux fins d'établissement ou de contestation de la filiation à moins que la communauté de vie ait cessé entre le couple quand il y a eu recours ; qu'en ne recherchant pas si Mme X... et M. Y... vivaient ensemble lorsque l'insémination a été pratiquée, la cour d'appel a privé sa décision de base légale au regard de l'article 311-20 du code civil ; </p>
<p>Mais attendu que la cour d'appel a exactement rappelé, par motifs adoptés, que les dispositions des articles 311-19 et 311-20 du code civil n'étaient pas applicables à l'action en établissement judiciaire de la filiation à la suite d'une procréation médicalement assistée sans tiers donneur, ces textes ne régissant que les procréations médicalement assistées avec tiers donneur ; que le moyen est inopérant ; </p>
<p>PAR CES MOTIFS : </p>
<p>REJETTE le pourvoi ; </p>
<p>Condamne M. Y... aux dépens ; </p>
<p>Vu l'article 700 du code de procédure civile, rejette sa demande et le condamne à payer à Mme X... la somme de 3 000 euros ; </p>
<p>Ainsi fait et jugé par la Cour de cassation, première chambre civile, et prononcé par le président en son audience publique du seize mars deux mille seize. </p>
<br/>
<p>MOYENS ANNEXES au présent arrêt </p>
<p>Moyens produits par la SCP Piwnica et Molinié, avocat aux Conseils, pour M. Y... 
</p>
<p>PREMIER MOYEN DE CASSATION </p>
<p>Il est reproché à l'arrêt attaqué d'avoir dit que M. Y... est le père de A...
X..., né le 10 février 2007, </p>
<p>AUX MOTIFS QU'il est constant que M. Pierre Y... et Mme Stéphanie X... ont entretenu une relation sentimentale à compter de l'année 1997 ; qu'il résulte des pièces produites que M. Pierre Y... et Mme Stéphanie X... ont signé un " Consentement en vue d'insémination artificielle du couple ", que le 20 avril 2006, M. Pierre Y... a donné son accord pour la congélation de son sperme pour permettre à Mme Stéphanie X... de recourir à une procréation médicalement assistée ; que le laboratoire Eylau est depuis cette date en charge de la conservation des paillettes de sperme de M. Pierre Y..., que le 26 mai 2006, il a été procédé à une insémination intra utérine de Mme Stéphanie X... suivie pour infertilité, avec les paillettes de M. Y..., que le 12 septembre 2006, le docteur Jean-Marc Z... a certifié que Mme Stéphanie X... était enceinte fixant au 26 mai 2006 la date du début de grossesse et au 26 février 2007, celle de la date prévue de l'accouchement ; que l'enfant A...
X... est né le 10 février 2007 ; que même à supposer que Mme X... se soit engagée à ne jamais le solliciter étant marié et père de trois enfants, qu'il n'ait jamais eu de projet parental avec elle et qu'ils ne formaient plus un couple depuis 1999, ces circonstances ne permettent pas de retenir que le consentement de M. Y... à la congélation de son sperme pour permettre à Mme X... de bénéficier d'une procréation médicalement assistée, n'a pas été libre et qu'il est le fruit d'une pression psychologique alors que l'intéressé n'est jamais revenu sur son accord, que les parties ont échangé sur le choix du prénom de l'enfant (leur conversation électronique du 16 octobre 2006) ainsi que sur la fréquence des rencontres entre le père et le fils (leur conversation électronique du 10 mai 2008) et que M. Y... a même effectué des versements au profit de Mme X... postérieurement à la naissance de A...(19 juillet 2007 et 15 mai 2008, notamment) ; que l'appelant ne peut davantage être suivi lorsqu'il soutient que la preuve de sa paternité ne serait pas établie, Mme X... ayant pu avoir d'autres partenaires lors de la période légale de conception, alors que la mère de l'enfant était suivie pour infertilité et que sa grossesse est datée du jour de l'insémination artificielle pratiquée avec les paillettes de M. Y..., l'attestation du Docteur Jean-Marc Z... du 20 octobre 2014 indiquant qu'une conception naturelle est possible chez une femme subissant une procréation médicalement assistée " sous réserve que l'infertilité soit inexpliquée et ne soit pas due à une cause qui imposerait le recours impératif à une procréation médicalement assistée... " étant à cet égard inopérante ; que le jugement entrepris qui a dit que M. Pierre Y... est le père de A...
X... est en conséquence confirmé, </p>
<p>1) ALORS QU'une présomption de filiation n'est attachée par la loi à une procréation médicalement assistée que dans la mesure où elle a été pratiquée dans le cadre et en respectant les conditions fixées par la loi ; que la loi réserve l'assistance médicale à la procréation aux cas d'infertilité du couple ou pour éviter la transmission à l'enfant d'une maladie d'une particulière gravité ; qu'en ne recherchant pas si Mme X..., qui indiquait avoir interrompu volontairement, à la demande de M. Y..., une précédente grossesse, était éligible à la procréation médicalement assistée et pouvait se prévaloir de la présomption de filiation prévue par la loi, la cour d'appel a privé sa décision de base légale au regard des articles L2141-1 et suivants du code de la santé publique et 327 et suivants du code civil ; </p>
<p>2) ALORS QUE l'homme et la femme qui ont recours à une procréation médicalement assistée doivent former un couple, être animés d'un projet parental et vivre ensemble ; qu'en retenant que même à supposer que M. Y... et Mme X... n'aient pas eu de projet parental et se soient ainsi placés hors du cadre fixé par la loi, la présomption de filiation résultant du recours à une procréation médicalement assistée n'en trouvait pas moins à s'appliquer, la cour d'appel a violé les articles L2141-1 et suivants du code de la santé publique et 327 et suivants du code civil ; </p>
<p>3) ALORS QU'à défaut d'être établie par la présomption résultant du recours régulier à une procréation médicalement assistée, la filiation, hors mariage, s'établit et se conteste par tous moyens ; que la cour d'appel a refusé d'envisager la possibilité d'une conception naturelle évoquée par le Dr Z..., la considérant comme « inopérante » compte tenu de la stérilité de Mme X... ; qu'en s'abstenant de rechercher si Mme X..., qui indiquait avoir déjà été enceinte, ne pouvait pas avoir conçu A... de façon naturelle, de sorte que cette éventualité était opérante et devait être examinée, au besoin en ordonnant une expertise pour trancher la filiation de l'enfant, la cour d'appel a violé les articles 327 et suivants du code civil. </p>
<p>SECOND MOYEN DE CASSATION (subsidiaire) </p>
<p>Il est reproché à l'arrêt attaqué d'avoir dit que M. Y... est le père de A...
X..., né le 10 février 2007, </p>
<p>AUX MOTIFS QU'il est constant que M. Pierre Y... et Mme Stéphanie X... ont entretenu une relation sentimentale à compter de l'année 1997 ; qu'il résulte des pièces produites que M. Pierre Y... et Mme Stéphanie X... ont signé un " Consentement en vue d'insémination artificielle du couple ", que le 20 avril 2006, M. Pierre Y... a donné son accord pour la congélation de son sperme pour permettre à Mme Stéphanie X... de recourir à une procréation médicalement assistée ; que le laboratoire Eylau est depuis cette date en charge de la conservation des paillettes de sperme de M. Pierre Y..., que le 26 mai 2006, il a été procédé à une insémination intra utérine de Mme Stéphanie X... suivie pour infertilité avec les paillettes de M. Y..., que le 12 septembre 2006, le docteur Jean-Marc Z... a certifié que Mme Stéphanie X... était enceinte fixant au 26 mai 2006 la date du début de grossesse et au 26 février 2007, celle de la date prévue de l'accouchement ; que l'enfant A...
X... est né le 10 février 2007 ; que même à supposer que Mme X... se soit engagée à ne jamais le solliciter étant marié et père de trois enfants, qu'il n'ait jamais eu de projet parental avec elle et qu'ils ne formaient plus un couple depuis 1999, ces circonstances ne permettent pas de retenir que le consentement de M. Y... à la congélation de son sperme pour permettre à Mme X... de bénéficier d'une procréation médicalement assistée, n'a pas été libre et qu'il est le fruit d'une pression psychologique alors que l'intéressé n'est jamais revenu sur son accord, que les parties ont échangé sur le choix du prénom de l'enfant (leur conversation électronique du 16 octobre 2006) ainsi que sur la fréquence des rencontres entre le père et le fils (leur conversation électronique du 10 mai 2008) et que M. Y... a même effectué des versements au profit de Mme X... postérieurement à la naissance de A...(19 juillet 2007 et 15 mai 2008, notamment) ; que l'appelant ne peut davantage être suivi lorsqu'il soutient que la preuve de sa paternité ne serait pas établie, Mme X... ayant pu avoir d'autres partenaires lors de la période légale de conception, alors que la mère de l'enfant était suivie pour infertilité et que sa grossesse est datée du jour de l'insémination artificielle pratiquée avec les paillettes de M. Y..., l'attestation du Docteur Jean-Marc Z... du 20 octobre 2014 indiquant qu'une conception naturelle est possible chez une femme subissant une procréation médicalement assistée " sous réserve que l'infertilité soit inexpliquée et ne soit pas due à une cause qui imposerait le recours impératif à une procréation médicalement assistée... " étant à cet égard inopérante ; que le jugement entrepris qui a dit que M. Pierre Y... est le père de A...
X... est en conséquence confirmé, </p>
<p>1) ALORS QUE le consentement donné à une procréation médicalement assistée interdit toute action aux fins d'établissement ou de contestation de la filiation à moins qu'il ne soit soutenu que l'enfant n'en est pas issu ; que M. Y... contestait que l'enfant soit issu de l'insémination litigieuse ; qu'en retenant qu'il était le père de l'enfant sans rechercher, s'il était issu de la procréation médicalement assistée, la cour d'appel a privé sa décision de base légale au regard de l'article 311-20 du code civil ; </p>
<p>2) ALORS QUE le consentement donné à une procréation médicalement assistée interdit toute action aux fins d'établissement ou de contestation de la filiation à moins que la communauté de vie ait cessé entre le couple quand il y a eu recours ; qu'en ne recherchant pas si Mme X... et M. Y... vivaient ensemble lorsque l'insémination a été pratiquée, la cour d'appel a privé sa décision de base légale au regard de l'article 311-20 du code civil.</p>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="1" TYPE="PRINCIPAL">SANTE PUBLIQUE - Assistance médicale à la procréation -  Assistance médicale à la procréation sans tiers donneur -  Etablissement judiciaire de la filiation -  Possibilité
</SCT>
<SCT ID="1" TYPE="REFERENCE">SANTE PUBLIQUE -  Assistance médicale à la procréation -  Assistance médicale à la procréation sans tiers donneur -  Articles 311-19 et 311-20 du code civil -  Application -  Exclusion -  Portée FILIATION -  Actions relatives à la filiation -  Actions aux fins d'établissement de la filiation -  Recevabilité -  Cas -  Assistance médicale à la procréation sans tiers donneur
</SCT>
<ANA ID="1">Les dispositions des articles 311-19 et 311-20 du code civil ne sont pas applicables à l'action en établissement judiciaire de la filiation à la suite d'une procréation médicalement assistée sans tiers donneur, ces textes ne régissant que les procréations médicalement assistées avec tiers donneur</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>



</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS>
<LIEN cidtexte="" datesignatexte="" id="" naturetexte="" nortexte="" num="" numtexte="" sens="source" typelien="CITATION">articles 311-19 et 311-20 du code civil</LIEN>
</LIENS>
</TEXTE_JURI_JUDI>
