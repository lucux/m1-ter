<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_JUDI>
<META>
<META_COMMUN>
<ID>JURITEXT000034550235</ID>
<ANCIEN_ID/>
<ORIGINE>JURI</ORIGINE>
<URL>texte/juri/judi/JURI/TEXT/00/00/34/55/02/JURITEXT000034550235.xml</URL>
<NATURE>ARRET</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Cour de cassation, civile, Chambre civile 2, 27 avril 2017, 17-40.027, Publié au bulletin</TITRE>
<DATE_DEC>2017-04-27</DATE_DEC>
<JURIDICTION>Cour de cassation</JURIDICTION>
<NUMERO>21700686</NUMERO>
<SOLUTION>QPC - Non-lieu à renvoi au Conseil constitutionnel</SOLUTION>
</META_JURI>
<META_JURI_JUDI>
<NUMEROS_AFFAIRES>
<NUMERO_AFFAIRE>17-40027</NUMERO_AFFAIRE>
</NUMEROS_AFFAIRES>
<PUBLI_BULL publie="oui"/>
<FORMATION>CHAMBRE_CIVILE_2</FORMATION>

<FORM_DEC_ATT>Cour d'appel de Paris</FORM_DEC_ATT>
<DATE_DEC_ATT>2017-02-07</DATE_DEC_ATT>
<SIEGE_APPEL/>
<JURI_PREM/>
<LIEU_PREM/>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme Flise</PRESIDENT>
<AVOCAT_GL>M. Lavigne</AVOCAT_GL>
<AVOCATS>SCP Nicolaÿ, de Lanouvelle et Hannotin, SCP Rocheteau et Uzan-Sarano, SCP Rousseau et Tapie</AVOCATS>
<RAPPORTEUR>Mme Vannier</RAPPORTEUR>
<ECLI>ECLI:FR:CCASS:2017:C200686</ECLI>
</META_JURI_JUDI>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>LA COUR DE CASSATION, DEUXIÈME CHAMBRE CIVILE, a rendu l'arrêt suivant : <br/>
<br/>
<br/>Reçoit la société Cardif Lux Vie en son intervention volontaire accessoire ; <br/>
<br/>Attendu que Mme Jacqueline X... et Mme Céline-Laure X... ont, respectivement les 13 décembre 2003 et 16 novembre 2007, souscrit un contrat d'assurance sur la vie en unités de compte auprès de la société Generali Vie (l'assureur) ; que, se prévalant du manquement de l'assureur à son obligation précontractuelle d'information, elles ont, la première le 15 mai 2012, la seconde le 23 mai 2012, exercé la faculté prorogée de renonciation que leur ouvraient respectivement les articles L. 132-5-1 et L. 132-5-2 du code des assurances ; que l'assureur ne leur ayant pas restitué les sommes qu'elles avaient versées, elles l'ont assigné en exécution de ses obligations ; que, devant la cour d'appel de Paris, Mmes X... ont présenté, par un écrit distinct et motivé, une question prioritaire de constitutionnalité ; <br/>
<br/>Attendu que la question transmise est ainsi rédigée : <br/>
<br/>« Sur la conformité de l'article L. 132-5-1 (ancien) du code des assurances dans sa rédaction issue de la loi n° 94-5 du 4 janvier 1994, applicable aux faits de la cause, et de l'article L. 132-5-2 (ancien) du code des assurances dans sa rédaction issue de la loi n° 2005-1564 du 15 décembre 2005, tels qu'interprétés par la jurisprudence de la Cour de cassation issue des arrêts de revirement du 19 mai 2016, aux droits et libertés garantis par la Constitution, en particulier au principe d'intelligibilité de la loi, à la garantie des droits et au principe de la liberté contractuelle et du droit au maintien des conventions et contrats légalement conclus, découlant des articles 4, 5, 6 et 16 de la Déclaration des droits de l'homme et du citoyen de 1789 » ; que cette question a été transmise à la Cour de cassation qui l'a reçue le 10 février 2017 ; <br/>
<br/>Attendu que ces textes sont applicables au litige qui est relatif aux effets de l'exercice, par le souscripteur d'un contrat d'assurance sur la vie, de la faculté prorogée de renonciation ; qu'ils n'ont pas déjà été déclarés conformes à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel ; <br/>
<br/>Mais attendu, d'abord, que la question, ne portant pas sur l'interprétation d'une disposition constitutionnelle dont le Conseil constitutionnel n'aurait pas encore eu l'occasion de faire application, n'est pas nouvelle ; <br/>
<br/>Et attendu, ensuite, que la méconnaissance de l'objectif de valeur constitutionnelle d'intelligibilité et d'accessibilité de la loi qui découle des articles 4, 5, 6 et 16 de la Déclaration des droits de l'homme et du citoyen de 1789 et impose au législateur d'adopter des dispositions suffisamment précises et des formules non équivoques, n'étant visée qu'en elle-même dans la question soulevée par le mémoire produit devant la juridiction qui l'a transmise et les moyens additionnels tendant à en préciser la portée présentés pour la première fois devant la Cour de cassation étant irrecevables, cette méconnaissance ne peut être invoquée à l'appui d'une question prioritaire de constitutionnalité sur le fondement de l'article 61-1 de la Constitution ; <br/>
<br/>Et attendu, encore, que l'exercice de la faculté prorogée de renonciation prévue à l'article L. 132-5-1 du code des assurances dans sa rédaction issue de la loi n° 94-5 du 4 janvier 1994 et à l'article L. 132-5-2 du même code dans sa rédaction issue de la loi n° 2005-1564 du 15 décembre 2005 en l'absence de respect par l'assureur du formalisme informatif édicté par ces textes répond à l'objectif de protection des consommateurs en leur permettant d'obtenir les informations nécessaires pour choisir le contrat convenant le mieux à leurs besoins pour profiter d'une concurrence accrue dans un marché unique de l'assurance ; que la portée effective conférée à ces dispositions par la jurisprudence constante de la Cour de cassation à laquelle se réfère la question, qui conduit à priver d'efficacité une renonciation déjà effectuée lorsqu'il est établi que l'exercice de cette prérogative a été détourné de sa finalité, garantit le respect du principe général de loyauté s'imposant aux contractants ; que, dans la mesure où elle repose sur un motif d'intérêt général en rapport direct avec le but poursuivi par le législateur, il ne peut être sérieusement soutenu qu'elle affecte une situation légalement acquise dans des conditions contraires à la garantie des droits proclamée par l'article 16 de la Déclaration des droits de l'homme et du citoyen ; <br/>
<br/>Et attendu, enfin, qu'il ne peut être sérieusement soutenu que la portée effective conférée à ces dispositions par la jurisprudence constante de la Cour de cassation, qui prive d'effet la renonciation exercée contrairement à sa finalité et laisse ainsi subsister le contrat, mais qui préserve les effets de cette renonciation lorsqu'elle est exercée conformément à sa finalité par un souscripteur qui, insuffisamment informé, n'a pas été en mesure d'apprécier la portée de son engagement, porte atteinte au droit au maintien des contrats légalement conclus ou à la liberté contractuelle qui découlent des articles 4 et 16 de la Déclaration des droits de l'homme et du citoyen ; <br/>
<br/>D'où il suit qu'il n'y a pas lieu de renvoyer la question au Conseil constitutionnel ; <br/>
<br/>PAR CES MOTIFS : <br/>
<br/>DIT N'Y AVOIR LIEU DE RENVOYER au Conseil constitutionnel la question prioritaire de constitutionnalité ; <br/>
<br/>Dit n'y avoir lieu à application de l'article 700 du code de procédure civile ; <br/>
<br/>Ainsi fait et jugé par la Cour de cassation, deuxième chambre civile, et prononcé par le président en son audience publique du vingt-sept avril deux mille dix-sept.
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="1" TYPE="PRINCIPAL">QUESTION PRIORITAIRE DE CONSTITUTIONNALITE - Assurance -  Code des assurances -  Articles L. 132-5-1 et L. 132-5-2 anciens -  Jurisprudence constante -  Applicabilité au litige -  Objectif de valeur constitutionnelle d'intelligibilité et d'accessibilité de la loi -  Invocabilité à l'appui d'une QPC -  Droit au maintien des conventions et contrats légalement conclus -  Liberté contractuelle -  Caractère sérieux - Défaut -  Non-lieu à renvoi au Conseil constitutionnel
</SCT>
<SCT ID="1" TYPE="REFERENCE"/>
<ANA ID="1"/>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>



</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_JUDI>
