<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_JUDI>
<META>
<META_COMMUN>
<ID>JURITEXT000033345248</ID>
<ANCIEN_ID/>
<ORIGINE>JURI</ORIGINE>
<URL>texte/juri/judi/JURI/TEXT/00/00/33/34/52/JURITEXT000033345248.xml</URL>
<NATURE>ARRET</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Cour de cassation, criminelle, Chambre criminelle, 2 novembre 2016, 15-87.163, Publié au bulletin</TITRE>
<DATE_DEC>2016-11-02</DATE_DEC>
<JURIDICTION>Cour de cassation</JURIDICTION>
<NUMERO>C1604746</NUMERO>
<SOLUTION>Cassation et désignation de juridiction</SOLUTION>
</META_JURI>
<META_JURI_JUDI>
<NUMEROS_AFFAIRES>
<NUMERO_AFFAIRE>15-87163</NUMERO_AFFAIRE>
</NUMEROS_AFFAIRES>
<PUBLI_BULL publie="oui"/>
<FORMATION>CHAMBRE_CRIMINELLE</FORMATION>

<FORM_DEC_ATT>Cour d'appel de Paris</FORM_DEC_ATT>
<DATE_DEC_ATT>2015-10-22</DATE_DEC_ATT>
<SIEGE_APPEL/>
<JURI_PREM/>
<LIEU_PREM/>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Guérin</PRESIDENT>
<AVOCAT_GL>M. Cordier</AVOCAT_GL>
<AVOCATS>SCP Foussard et Froger</AVOCATS>
<RAPPORTEUR>M. Bonnal</RAPPORTEUR>
<ECLI>ECLI:FR:CCASS:2016:CR04746</ECLI>
</META_JURI_JUDI>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>LA COUR DE CASSATION, CHAMBRE CRIMINELLE, a rendu l'arrêt suivant : <p>Statuant sur le pourvoi formé par :</p>
<p>- M. Louis X..., partie civile, </p>
<p>contre l'arrêt de la cour d'appel de PARIS, chambre 2-7, en date du 22 octobre 2015, qui, dans la procédure suivie contre M. Henri Y... du chef de diffamation publique envers un fonctionnaire public, a constaté l'extinction de l'action publique par prescription ; </p>
<p>La COUR, statuant après débats en l'audience publique du 20 septembre 2016 où étaient présents : M. Guérin, président, M. Bonnal, conseiller rapporteur, MM. Straehli, Buisson, Mme Durin-Karsenty, MM. Larmanjat, Ricard, Parlos, conseillers de la chambre, M. Ascensi, conseiller référendaire ; </p>
<p>Premier avocat général : M. Cordier ; </p>
<p>Greffier de chambre : Mme Zita ; </p>
<p>Sur le rapport de M. le conseiller BONNAL, les observations de la société civile professionnelle FOUSSARD et FROGER, avocat en la Cour, et les conclusions de M. premier avocat général CORDIER ; </p>
<p>Vu le mémoire produit ; </p>
<p>Sur le premier moyen de cassation, pris de la violation des articles 65 de la loi du 29 juillet 1881, 29 et 31 de la même loi, 591 et 593 du code de procédure pénale ; </p>
<p>" en ce que l'arrêt attaqué, infirmant le jugement du 25 septembre 2014, a constaté, en application de l'article 65 de la loi du 29 juillet 1881, la prescription de l'action publique et de l'action civile ; </p>
<p>" aux motifs que la cour relèvera tout d'abord, que si la Cour de cassation a dit n'y avoir lieu à avis, elle a, néanmoins, préalablement largement éclairé le champ du débat tant au plan technique que juridique ; qu'en ce qui concerne l'aspect technique, c'est par référence à cette analyse que le premier juge a qualifié l'hyperlien créé par l'appelant d'« activable, profond et interne » ; que lien « activable » signifie que son accès dépend de la volonté de l'internaute qui consulte le site « source » ; que lien « profond » suppose un accès direct à une information, sans passer par l'accueil du site « cible » ; que lien « interne » est une notion qui renvoie au fait que le lien propose une connexion à une « collection » homogène ; que ces différents éléments sont en l'espèce acquis, notamment, sur le dernier point qui renvoie à la maîtrise par M. Y... des différents sites considérés ; qu'il convient cependant de souligner que la Cour de cassation a motivé son refus d'avis par le fait que le tribunal n'a pas dans sa question intégré la distinction entre hyperlien interne ou externe qui demeure une question d'espèce ; que la réponse de la Cour de cassation a encore rappelé les différentes jurisprudences et le cadre légal qui ont amené à exclure que les publications en ligne soient assujetties, notamment en matière de prescription, à un régime plus sévère que celui du livre ou de la presse « papier » ; qu'en parallèle avec la notion de réédition sur support papier, une nouvelle mise en ligne d'un texte ou d'une vidéo ne ferait donc courir un nouveau délai de prescription que s'il manifeste la volonté de publication nouvelle de son auteur ; que selon le tribunal, tel était bien le cas en l'espèce, cette volonté étant caractérisée par le fait de citer « l'inspecteur X... » dans un article qui ne lui était pas consacré et d'inviter explicitement les internautes à se reporter aux pages le concernant grâce au lien inséré par le prévenu dans le nouveau texte renvoyant au propos plus ancien visé par la prévention, dont il est également l'auteur ; que M. X... a repris à son compte l'analyse du tribunal relative à l'avis de la Cour de cassation, insistant sur une conclusion qui donne toute liberté d'appréciation au juge du fond ; qu'il adhère encore à la constatation par le premier juge d'une volonté manifeste de M. Y... de procéder à une publication nouvelle, faisant courir un nouveau délai de prescription ; que le prévenu dans ses écritures, s'est déclaré « déçu » par l'avis ci-avant détaillé de la Cour de cassation, en ce qu'il n'a pas donné la place qui convenait aux avis, selon lui majoritaires, qui considèrent que la création d'un hyperlien ne serait guère plus qu'un renvoi à « un ouvrage de bibliothèque » ; que le refus de donner un avis en ce sens résulterait à nouveau « du poids des services fiscaux » ; que le fait de ne pas tenir pour acquise la prescription serait pour lui, au-delà du cas d'espèce, une grave entrave à la liberté d'expression, que la cour considérera que les caractéristiques techniques de l'hyperlien créé par M. Y... dans son texte du 29 juin 2011, sont bien conformes à la description qu'en fait le premier juge, ci-avant rappelée ; qu'il sera encore relevé que c'est le caractère de « lien interne » qui semble avoir été déterminant de sa décision pour retenir qu'il s'agissait d'une nouvelle publication du propos du 26 mai 2010 ; que cependant, il apparaît que le texte nouveau, qui effectivement, ne concerne pas au premier chef M. X..., est néanmoins relatif aux démêlés du prévenu avec l'administration fiscale (la pertinence du propos n'ayant pas à ce stade à être appréciée) ; qu'ainsi le renvoi proposé à l'internaute de se référer à des textes antérieurs intéressant la même question n'est pas dépourvu de cohérence, la mise en cause de la partie civile n'étant pas gratuite, qu'elle soit ou non fondée ; qu'il sera encore noté que le texte du 29 juin 2011, ne fait que proposer au lecteur une information complémentaire ; qu'en effet, « l'inspecteur X... », n'est présenté que comme le subordonné de « M. Z... » et il est dit par le prévenu « Voir à ce sujet (...) avec le lien suivant » ; que cette offre n'est présentée ni comme indispensable, ni comme porteuse de renseignements essentiels ; qu'il s'agit seulement d'un élément pouvant compléter l'information d'un internaute ; que, par ailleurs, il n'est pas contesté le texte litigieux reste accessible directement par diverses adresses IP ou références et pourrait, à défaut de poursuites, faire l'objet de demandes de retrait ; qu'aussi le lien créé renvoie-t-il à un propos ancien et autonome qu'un internaute peut ou non consulter, et qui aurait pu être en son temps poursuivi ; que c'est pourquoi le prévenu est bien fondé à considérer que la poursuite dont il est l'objet est prescrite » ; </p>
<p>" 1°) alors que le fait de publier sur un site internet un nouveau texte renvoyant à un précédent article au moyen d'un hyperlien activable, profond et interne réalise la mise à disposition de cet article à un nouveau public ; qu'en tant que tel, l'insertion de ce lien hypertexte vaut nouvelle publication du texte auquel il renvoie ; qu'en l'espèce, la cour d'appel a retenu, comme les premiers juges, que l'hyperlien inséré le 29 juin 2011 par M. Y... était un lien activable par l'utilisateur, un lien profond renvoyant directement aux propos initiaux, et un lien interne renvoyant à un document accessible sur le même site de M. Y... ; qu'eu égard à ces constatations, les juges du second degré ont mis en évidence l'existence d'une nouvelle publication ; qu'en décidant le contraire, la cour d'appel n'a pas tiré les conséquences légales de ses propres constations, en violation des textes susvisés, et notamment de l'article 65 de la loi du 29 juillet 1881 ; </p>
<p>" 2°) alors qu'il importe peu que l'hyperlien vise à compléter l'information de l'internaute, ou que le texte originaire reste accessible directement par d'autres adresses, ou encore qu'il puisse faire l'objet d'une demande de retrait de la part de la personne diffamée ; qu'en statuant sur la base de telles considérations, la cour d'appel s'est prononcée par des motifs inopérants, en violation des textes susvisés et notamment de l'article 65 de la loi du 29 juillet 1881 ; </p>
<p>" 3°) et alors que, et en tout cas, en s'abstenant de s'expliquer sur le point de savoir si le fait que le lien hypertexte était activable, profond et interne n'était pas de nature à valoir nouvelle publication du texte auquel il renvoyait, la cour d'appel a à tout le moins privé sa décision de base légale au regard des textes susvisés, et notamment de l'article 65 de la loi du 29 juillet 1881 " ; </p>
<p>Vu l'article 65 de la loi du 29 juillet 1881 ; </p>
<p>Attendu qu'il résulte dudit article qu'en matière d'infractions à la loi sur la liberté de la presse, le point de départ de la prescription est le jour de la publication de l'écrit incriminé, par laquelle se consomment les délits que celui-ci peut contenir ; qu'il suit de là que toute reproduction, dans un écrit rendu public, d'un texte déjà publié, est constitutive d'une publication nouvelle dudit texte, qui fait courir un nouveau délai de prescription ; que l'insertion, sur internet, par l'auteur d'un écrit, d'un lien hypertexte renvoyant directement audit écrit, précédemment publié, caractérise une telle reproduction ; </p>
<p>Attendu qu'il résulte de l'arrêt attaqué et des pièces de la procédure que M. X..., inspecteur des impôts, a porté plainte et s'est constitué partie civile du chef de diffamation publique envers un fonctionnaire public en raison de la mise en ligne sur un site internet édité par M. Y..., le 29 juin 2011, du texte d'une citation à comparaître devant le tribunal correctionnel de Paris que celui-ci lui avait fait délivrer, texte directement accessible par un lien hypertexte inséré dans un article intitulé " La preuve par trois " ; que renvoyé devant le tribunal correctionnel, M. Y... a excipé de la prescription de l'action publique, au motif qu'il avait, le 26 mai 2010, rendu accessible la même citation à comparaître depuis un précédent article intitulé " L'enfer-Ici tout de suite " également mis en ligne sur un site internet qu'il éditait ; que les juges du premier degré ont écarté ce moyen et ont déclaré le prévenu coupable ; que celui-ci a relevé appel de cette décision ; </p>
<p>Attendu que, pour infirmer le jugement déféré et dire la prescription acquise, l'arrêt prononce par les motifs repris au moyen ; </p>
<p>Mais attendu qu'en statuant ainsi, alors que le texte incriminé avait été rendu à nouveau accessible par son auteur au moyen d'un lien hypertexte, y renvoyant directement, inséré dans un contexte éditorial nouveau, la cour d'appel a méconnu le texte susvisé et le principe ci-dessus énoncé ; </p>
<p>D'où il suit que la cassation est encourue ; </p>
<p>Par ces motifs et sans qu'il soit besoin d'examiner le second moyen de cassation proposé : </p>
<p>CASSE et ANNULE, en toutes ses dispositions, l'arrêt susvisé de la cour d'appel de Paris, en date du 22 octobre 2015, et pour qu'il soit à nouveau jugé, conformément à la loi, </p>
<p>RENVOIE la cause et les parties devant la cour d'appel de Versailles, à ce désignée par délibération spéciale prise en chambre du conseil ; </p>
<p>ORDONNE l'impression du présent arrêt, sa transcription sur les registres du greffe de la cour d'appel de Paris et sa mention en marge ou à la suite de l'arrêt annulé ; </p>
<p>Ainsi fait et jugé par la Cour de cassation, chambre criminelle, et prononcé par le président le deux novembre deux mille seize ; </p>
<p>En foi de quoi le présent arrêt a été signé par le président, le rapporteur et le greffier de chambre.</p>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="1" TYPE="PRINCIPAL">PRESSE - Procédure -  Action publique -  Extinction -  Prescription -  Délai -  Point de départ -  Diffusion sur le réseau internet -  Mise en ligne d'un lien hypertexte renvoyant à un texte déjà publié -  Nouvelle publication -  Conditions -  Détermination
</SCT>
<SCT ID="1" TYPE="REFERENCE">ACTION PUBLIQUE -  Extinction -  Prescription -  Délai -  Point de départ -  Presse -  Diffusion sur le réseau internet -  Mise en ligne d'un lien hypertexte renvoyant à un texte déjà publié -  Nouvelle publication -  Conditions -  Détermination PRESCRIPTION -  Action publique -  Délai -  Point de départ -  Presse -  Diffusion sur le réseau internet -  Mise en ligne d'un lien hypertexte renvoyant à un texte déjà publié -  Nouvelle publication -  Conditions -  Détermination
</SCT>
<ANA ID="1">L'insertion, sur internet, par l'auteur d'un écrit, d'un lien hypertexte renvoyant directement audit écrit, précédemment publié, caractérise une reproduction, à nouveau rendue publique, d'un texte déjà publié et est constitutive d'une publication nouvelle dudit texte, qui fait courir à nouveau le délai de prescription de l'article 65 de la loi du 29 juillet 1881 sur la liberté de la presse</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>Sur la reproduction d'un écrit déjà publié, en ce qu'elle constitue une nouvelle infraction, à rapprocher :Crim., 2 octobre 2012, pourvoi n° 12-80.419, Bull. crim. 2012, n° 204 (rejet), et les arrêts citésSur le nouvel acte de publication en cas de diffusion sur le réseau internet, à rapprocher :Avis de la Cour de cassation, 26 mai 2014, n° 14-70.004, Bull. crim. 2014, Avis, n° 3 (non-lieu à avis), et l'arrêt cité


</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS>
<LIEN cidtexte="" datesignatexte="" id="" naturetexte="" nortexte="" num="" numtexte="" sens="source" typelien="CITATION">article 65 de la loi du 29 juillet 1881</LIEN>
</LIENS>
</TEXTE_JURI_JUDI>
