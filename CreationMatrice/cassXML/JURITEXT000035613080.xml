<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_JUDI>
<META>
<META_COMMUN>
<ID>JURITEXT000035613080</ID>
<ANCIEN_ID/>
<ORIGINE>JURI</ORIGINE>
<URL>texte/juri/judi/JURI/TEXT/00/00/35/61/30/JURITEXT000035613080.xml</URL>
<NATURE>ARRET</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Cour de cassation, civile, Chambre commerciale, 20 septembre 2017, 14-17.225, Publié au bulletin</TITRE>
<DATE_DEC>2017-09-20</DATE_DEC>
<JURIDICTION>Cour de cassation</JURIDICTION>
<NUMERO>41701190</NUMERO>
<SOLUTION>Cassation</SOLUTION>
</META_JURI>
<META_JURI_JUDI>
<NUMEROS_AFFAIRES>
<NUMERO_AFFAIRE>14-17225</NUMERO_AFFAIRE>
</NUMEROS_AFFAIRES>
<PUBLI_BULL publie="oui"/>
<FORMATION>CHAMBRE_COMMERCIALE</FORMATION>

<FORM_DEC_ATT>Cour d'appel de Bordeaux</FORM_DEC_ATT>
<DATE_DEC_ATT>2014-03-13</DATE_DEC_ATT>
<SIEGE_APPEL/>
<JURI_PREM/>
<LIEU_PREM/>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme Mouillard (président)</PRESIDENT>
<AVOCAT_GL/>
<AVOCATS>Me Carbonnier, SCP Célice, Soltner, Texidor et Périer</AVOCATS>
<RAPPORTEUR/>
<ECLI>ECLI:FR:CCASS:2017:CO01190</ECLI>
</META_JURI_JUDI>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>LA COUR DE CASSATION, CHAMBRE COMMERCIALE, a rendu l'arrêt suivant : <br/>
<br/>
<br/> 	Attendu, selon l'arrêt attaqué, que, le 23 mars 2007, la société Pharmacie Bérard Argivier (la société Pharmacie) est devenue sociétaire de la coopérative Astera (la coopérative) et s'est engagée, à ce titre, à passer des opérations avec la coopérative dans les conditions définies par son règlement intérieur prévoyant que la coopérative fournit aux sociétaires, directement ou indirectement par l'intermédiaire de la société Compagnie d'exploitation et de répartition pharmaceutiques de Rouen (la société CERP), les produits pharmaceutiques et para-pharmaceutiques ainsi que les marchandises nécessaires à l'exercice de leur activité ; que la société Pharmacie a signé, le même jour, une « convention-cadre de vente de marchandises sous réserve de propriété » avec la société CERP stipulant une clause de réserve de propriété et prévoyant que l'ensemble des marchandises livrées devrait être réglé conformément au « règlement intérieur » de la société Cerp faisant partie intégrante de la convention-cadre ; que, le 16 août 2011, la société Pharmacie a été mise en sauvegarde, la société Malmezat-Prat étant désignée mandataire judiciaire, tandis qu'aucun administrateur judiciaire n'a été nommé ; que, la société CERP souhaitant revenir sur les termes de la convention-cadre en exigeant des modalités de paiement de ses factures plus contraignantes, la société Pharmacie, sur avis conforme de la société Malmezat-Prat, ès qualités, a saisi, le 2 décembre 2011, le juge-commissaire pour être autorisée à poursuivre le contrat d'approvisionnement du 23 mars 2007 dans les conditions contractuelles initiales ; <br/>
<br/> 	Sur le moyen unique, pris en sa première branche :<br/>
<br/> 	Attendu que la société CERP fait grief à l'arrêt d'ordonner la continuation du contrat du 23 mars 2007, selon les modalités prévues par celui-ci, sous astreinte de 500 euros par jour pour toute inexécution, alors, selon le moyen, que la notion de contrat en cours, au sens de l'article L. 622-13 du code de commerce, suppose qu'une prestation soit due par le cocontractant du débiteur ; qu'en jugeant que la convention du 23 mars 2007, ayant pour objet de fixer les modalités applicables aux commandes passées par la société Pharmacie auprès de la société CERP, constituaient un contrat en cours, sans caractériser l'existence d'une prestation déterminée qui aurait incombé à la société CERP en application de cette convention, la cour d'appel a privé sa décision de base légale au regard des articles L. 622-13 et L. 627-2 du code de commerce ;<br/>
<br/> 	Mais attendu que l'arrêt constate que la société Pharmacie avait, le même jour, souscrit des parts sociales de la coopérative et conclu avec la société CERP un contrat-cadre de vente de marchandises stipulant que le transfert de leur propriété ne serait réalisé qu'après complet paiement de leur prix conformément au règlement intérieur de la société CERP, selon lequel la coopérative fournit en produits pharmaceutiques ou para-pharmaceutiques les adhérents directement ou indirectement par l'intermédiaire de la société CERP, tandis que les adhérents s'engagent à réaliser directement auprès de la coopérative ou indirectement auprès de la société CERP un chiffre d'affaires mensuel minimum, sous la sanction de l'exclusion de la coopérative ; qu'en l'état de ces constatations, faisant ressortir que la société Pharmacie était tenue d'une obligation de s'approvisionner auprès de la société CERP selon un volume déterminé et que la société CERP était elle-même tenue, par la convention-cadre, de livrer les produits commandés, ce dont il résulte qu'il existait entre les parties un contrat d'approvisionnement dont les effets n'étaient pas épuisés au jour de l'ouverture de la procédure de sauvegarde, la cour d'appel a caractérisé l'existence d'un contrat en cours au sens de l'article L. 622-13, I, du code de commerce et ainsi légalement justifié sa décision ; que le moyen n'est pas fondé ;<br/>
<br/> 	Mais sur le moyen, pris en sa seconde branche :<br/>
<br/> 	Vu l'article L. 622-13, II, alinéa 2, du code de commerce, dans sa rédaction antérieure à l'ordonnance du 12 mars 2014 ;<br/>
<br/> 	Attendu que lorsque la prestation que doit le débiteur dans le cadre de l'exécution d'un contrat en cours porte sur le paiement d'une somme d'argent, celui-ci doit se faire au comptant ; que la seule exception à cette règle consiste dans l'acceptation, par le cocontractant du débiteur, de délais de paiement, ce qui exclut, en cas de refus du cocontractant, toute survivance de tels délais convenus entre les parties dans le contrat en cours dont l'exécution est exigée ;<br/>
<br/> 	Attendu que pour statuer comme il fait, l'arrêt retient que les modalités de paiement différé des factures faisant partie intégrante du contrat, reconduit dans son intégralité, ne sauraient être remises en cause ;<br/>
<br/> 	Qu'en statuant ainsi, la cour d'appel a violé le texte susvisé ;<br/>
<br/> 	PAR CES MOTIFS :<br/>
<br/> 	CASSE ET ANNULE, en toutes ses dispositions, l'arrêt rendu le 13 mars 2014, entre les parties, par la cour d'appel de Bordeaux ; remet, en conséquence, la cause et les parties dans l'état où elles se trouvaient avant ledit arrêt et, pour être fait droit, les renvoie devant la cour d'appel de Poitiers ;<br/>
<br/> 	Condamne la société Pharmacie Bérard Argivier et la société Malmezat Prat, en sa qualité de commissaire à l'exécution du plan de la société Pharmacie Bérard Argivier, aux dépens ;<br/>
<br/> 	Vu l'article 700 du code de procédure civile, rejette les demandes ;<br/>
<br/> 	Dit que sur les diligences du procureur général près la Cour de cassation, le présent arrêt sera transmis pour être transcrit en marge ou à la suite de l'arrêt cassé ;<br/>
<br/> 	Ainsi fait et jugé par la Cour de cassation, chambre commerciale, financière et économique, et prononcé par le président en son audience publique du vingt septembre deux mille dix-sept.<br/>MOYEN ANNEXE au présent arrêt<br/>
<br/> Moyen produit par la SCP Célice, Soltner, Texidor et Périer, avocat aux Conseils, pour la société Compagnie d'exploitation et de répartition pharmaceutiques de Rouen.<br/>
<br/> Il est fait grief à l'arrêt confirmatif attaqué d'avoir ordonné la continuation de la convention du 23 mars 2007, selon les modalités prévues par cette convention, sous astreinte de 500 euros par jour pour toute inexécution ;<br/>
<br/> Aux motifs propres que « la SAS CERP conclut à l'absence de contrat de d'approvisionnement en cours entre les parties, les relations existant entre elles constituant des ventes successives et le pharmacien étant libre d'acheter ses produits auprès d'autres fournisseurs ; que l'intimée fait au contraire valoir qu'il existe entre elles un contrat d'approvisionnement en cours dont la SAS CERP a voulu modifier les conditions financières ; que le 23 mars 2007, d'une part la SELARL BERARD ARGIVIER selon un bulletin d'adhésion et de souscription s'est inscrite en qualité de sociétaire de la SAS CERP ROUEN, a reconnu avoir pris connaissance et accepté sans réserves les dispositions du règlement intérieur de la CERP ROUEN et s'est engagée à souscrire parts sociales ; que d'autre part elle a signé une « convention cadre de marchandises sous réserve de propriété » par lequel les parties ont convenu que le transfert de propriété des marchandises ne sera réalisé qu'après complet paiement du prix effectif en principal et accessoires et ce quelle que soit la date de livraison des marchandises ; que cette convention précise les modalités de passation des commandes et indique, entre autres dispositions, que les marchandises devront être payées conformément au règlement intérieur de la CERP qui fait partie intégrante de la convention ; que ce règlement intérieur certes établi au nom d'ASTERA et qui a été communiqué par l'appelante à l'appui de sa déclaration de créance a pour objet de régler les rapports entre la coopérative et ses sociétaires et notamment les conditions générales de vente ; qu'il précise que la coopérative fournit les sociétaires coopérateurs directement ou indirectement par l'intermédiaire de CERP en produits pharmaceutiques et parapharmaceutiques à usage humain ou vétérinaire ainsi qu'en marchandises, denrées ou services, équipements et matériels nécessaires à leur activité, que chaque sociétaire s'engage à réaliser directement auprès de la coopérative ou indirectement auprès de la CERP un chiffre d'affaires HT mensuel minimum de 7000 € sous peine de mise en oeuvre de l'article 12 des statuts (l'exclusion de la coopérative) ; que les conditions générales de vente précisent que toute passation de commande implique l'adhésion à ASTERA COOPERATIVE, déterminent notamment les modalités de passation des commandes et de leur livraison, les modalités de règlement des factures, les conséquences des retards de paiement ; que cet ensemble contractuel qui s'inscrit dans la durée constitue à l'évidence un contrat synallagmatique d'approvisionnement à durée indéterminée et à exécution successive qui n'avait pas épuisé ses effets lors de l'ouverture de la procédure de sauvegarde et était donc en cours au sens de l'article L 622-13 du code de commerce ; que les conditions générales de vente prévoient que « les factures sont portées sur des relevés arrêtés selon une périodicité de huitaine (soit le 8, le 15, le 22 et en fin de mois ) et que le règlement net des relevés doit être effectué à 30 jours à fin de mois ou son équivalent à 41 jours » ; que la SAS CERP estime que ces dispositions contreviennent à l'article 622-13 2 du code de commerce et qu'un paiement comptant doit être instauré ; qu'or faisant partie intégrante du contrat elles ne sauraient être remises en cause ; qu'en conséquence le jugement déféré sera confirmé en toutes ses dispositions » (arrêt attaqué, p. 6, § 3 à p. 7, § 3) ;<br/>
<br/> Et aux motifs éventuellement adoptés du tribunal que « la déclaration de créance du 3 octobre 2011 de la Cerp Rouen fait notamment référence, au titre des pièces jointes, au bulletin d'adhésion à Cerp Rouen, à la clause de réserve de propriété et aux conditions générales de vente Cerp Rouen ; que selon les statuts de l'association coopérative "Astera les pharmaciens associés", adoptés par l'assemblée générale du juillet 2008, tout pharmacien exerçant son activité en tant que propriétaire d'une officine peut devenir sociétaire coopérateur et s'engage à traiter des opérations avec la coopérative dans les conditions définies par le règlement intérieur prévu à l'article 27 des statuts ; que le règlement intérieur précité, au chapitre des opérations réalisées par les sociétaires coopérateurs, prévoit que la coopérative fournit à ces derniers, directement ou indirectement par l'intermédiaire de Cerp Rouen SAS, les produits pharmaceutiques et para-pharmaceutiques à usage humain et vétérinaire, ainsi que les marchandises, denrées ou services, équipements et matériels nécessaires à l'exercice de leur activité ; qu'en outre, chaque sociétaire s'engage à réaliser directement auprès de la coopérative ou indirectement auprès de la Cerp Rouen un chiffre d'affaires hors taxe mensuel minimum de 7000 € ; que de même, selon les conditions générales de vente de Cerp Rouen, toute passation de commande implique l'adhésion à "Astera coopérative", et les factures sont portées sur des relevés arrêtés selon une périodicité de huitaine avec un règlement net des relevés effectué à 30 jours en fin de mois ou son équivalent à 41 jours de relevé ; qu'il résulte de l'examen de ces documents, un lien direct entre l'adhésion à la société coopérative et le fournisseur, principalement par le renvoi dans les statuts au règlement intérieur, alors même qu'il n'est nullement mentionné dans lesdits statuts la liberté pour le pharmacien adhérent de se faire livrer les médicaments par une autre société que Cerp Rouen, de sorte que le contrat liant la pharmacie Bérard-Argivier à la société Cerp Rouen ne peut être qualifié de contrat autonome ou successif, ainsi que le soutient l'opposant, mais d'un contrat en cours comme retenu par le juge-commissaire ; que par voie de conséquence, il convient de confirmer l'ordonnance déférée » (jugement entrepris, p. 3, pénult. § à p. 4, § 4) ;<br/>
<br/> Alors d'une part que la notion de contrat en cours, au sens de l'article L. 622-13 du code de commerce, suppose qu'une prestation soit due par le cocontractant du débiteur ; qu'en jugeant que la convention du 23 mars 2007, ayant pour objet de fixer les modalités applicables aux commandes passées par la Pharmacie Bérard Argivier auprès de la Cerp Rouen, constituaient un contrat en cours, sans caractériser l'existence d'une prestation déterminée qui aurait incombé à la Cerp Rouen en application de cette convention, la cour d'appel a privé sa décision de base légale au regard des articles L. 622-13 et L. 627-2 du code de commerce ;<br/>
<br/> Alors subsidiairement, d'autre part, qu'il résulte de l'article L. 622-13 II du code de commerce dans sa rédaction applicable à la cause que dans le cas où les relations contractuelles poursuivies impliquent le paiement d'une somme d'argent par le débiteur en procédure collective, ce paiement doit se faire au comptant, nonobstant les délais qui auraient pu être antérieurement convenus entre les parties ; qu'en retenant que les délais de paiement prévus au profit de la Pharmacie Bérard Argivier par la convention du 23 mars 2007 demeuraient applicables pour les commandes passées postérieurement au jugement d'ouverture, la cour d'appel a violé ledit article L. 622-13 II.
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP>
<CONTENU>



</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_JUDI>
