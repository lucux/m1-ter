<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_JUDI>
<META>
<META_COMMUN>
<ID>JURITEXT000032381334</ID>
<ANCIEN_ID/>
<ORIGINE>JURI</ORIGINE>
<URL>texte/juri/judi/JURI/TEXT/00/00/32/38/13/JURITEXT000032381334.xml</URL>
<NATURE>ARRET</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Cour de cassation, criminelle, Chambre criminelle, 5 avril 2016, 15-81.349, Publié au bulletin</TITRE>
<DATE_DEC>2016-04-05</DATE_DEC>
<JURIDICTION>Cour de cassation</JURIDICTION>
<NUMERO>C1601221</NUMERO>
<SOLUTION>Rejet</SOLUTION>
</META_JURI>
<META_JURI_JUDI>
<NUMEROS_AFFAIRES>
<NUMERO_AFFAIRE>15-81349</NUMERO_AFFAIRE>
</NUMEROS_AFFAIRES>
<PUBLI_BULL publie="oui">Bulletin criminel 2016, n° 120</PUBLI_BULL>
<FORMATION>CHAMBRE_CRIMINELLE</FORMATION>

<FORM_DEC_ATT>Cour d'appel de Besançon</FORM_DEC_ATT>
<DATE_DEC_ATT>2015-01-09</DATE_DEC_ATT>
<SIEGE_APPEL/>
<JURI_PREM/>
<LIEU_PREM/>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Guérin</PRESIDENT>
<AVOCAT_GL>M. Cuny</AVOCAT_GL>
<AVOCATS>SCP Rousseau et Tapie</AVOCATS>
<RAPPORTEUR>Mme Guého</RAPPORTEUR>
<ECLI>ECLI:FR:CCASS:2016:CR01221</ECLI>
</META_JURI_JUDI>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>LA COUR DE CASSATION, CHAMBRE CRIMINELLE, a rendu l'arrêt suivant : <p>Statuant sur le pourvoi formé par :</p>
<p>- La société GMF, partie intervenante, </p>
<p>contre l'arrêt de la cour d'appel de BESANÇON, chambre correctionnelle, en date du 9 janvier 2015, qui, sur renvoi après cassation (Crim., 4 mars 2014, n° 13-80. 472), dans la procédure suivie contre M. Ernest X...du chef de blessures involontaires aggravées, a prononcé sur les intérêts civils ; </p>
<p>La COUR, statuant après débats en l'audience publique du 16 février 2016 où étaient présents : M. Guérin, président, Mme Guého, conseiller rapporteur, Mmes Dreifuss-Netter, Schneider, Farrenq-Nési, M. Bellenger, conseillers de la chambre, Mme Harel-Dutirou, conseiller référendaire ; </p>
<p>Avocat général : M. Cuny ; </p>
<p>Greffier de chambre : Mme Zita ; </p>
<p>Sur le rapport de Mme le conseiller référendaire GUÉHO, les observations de la société civile professionnelle ROUSSEAU et TAPIE, avocat en la Cour, et les conclusions de M. l'avocat général CUNY ; </p>
<p>Vu le mémoire produit ; </p>
<p>Sur le moyen unique de cassation, pris de la violation des articles 1382 du code civil, 427, 591 et 593 du code de procédure pénale, défaut de motifs, manque de base légale ; </p>
<p>" en ce que l'arrêt attaqué a fixé à la somme de 164 413 euros la perte de gains professionnels futurs subie par M. B... à la suite de l'accident dont il a été victime le 23 septembre 2005 et a condamné la GMF à payer à M. B..., en réparation de ce préjudice, la somme de 137 716, 24 euros ; </p>
<p>" aux motifs qu'en appliquant le barème de capitalisation édité par la Gazette du Palais en mars 2013, et en retenant ainsi, compte tenu de l'âge de M. B... (62 ans), un euro de rente de 2, 857, le montant capitalisé de la perte de revenus jusqu'à la retraite s'établissait à 56 848, 59 euros, soit compte tenu du coefficient de perte de chance retenu, un montant de 45 479 euros ; qu'en définitive, la perte de gains professionnels futurs s'établissait à un total de 164 413 euros dont à déduire les arrérages échus et à échoir de la rente d'invalidité ; </p>
<p>" 1°) alors que l'objet de la responsabilité civile est de rétablir, aussi exactement que possible, l'équilibre détruit par le dommage et de replacer la victime dans la situation où elle se serait trouvée si l'acte dommageable n'avait pas eu lieu, sans qu'il résulte pour elle perte ou profit ; que la victime choisissant d'être indemnisée de ses préjudices futurs par l'allocation d'un capital, dont le versement est libératoire pour le responsable ou son assureur, ne peut obtenir d'indemnisation au titre des événements futurs, telle l'inflation, susceptibles d'affecter le rendement ultérieur de ce capital ; qu'en fixant le montant des préjudices subis par M. B... sur la base d'un barème de capitalisation tenant compte d'un taux d'inflation future, majorant ainsi le montant du capital alloué à la victime, la cour d'appel a violé l'article 1382 du code civil, ensemble le principe de réparation intégrale du préjudice sans perte ni profit ; </p>
<p>" 2°) alors que seul est indemnisable le préjudice ayant un lien de causalité direct avec le fait dommageable ; que l'inflation susceptible de survenir postérieurement à la décision fixant le montant du préjudice de la victime constitue un événement sans rapport aucun de causalité directe avec le fait dommageable source de responsabilité ; qu'en fixant le montant des préjudices subis par M. B... en tenant compte de l'inflation future, quand cet événement aléatoire, lié au seul contexte économique, ne revêtait pas de lien de causalité direct avec l'accident dont avait été victime M. B..., les juges d'appel ont violé l'article 1382 du code civil, ensemble le principe de réparation intégrale du préjudice sans perte ni profit ; </p>
<p>" 3°) alors que le préjudice futur n'est indemnisable qu'à la condition de revêtir un caractère certain, ce qui implique qu'il constitue la prolongation directe et certaine d'un état de fait actuel ; qu'en liquidant les préjudices subis par M. B... sur la base d'un barème de capitalisation tenant compte de l'érosion monétaire future, calculée sur la base d'une projection de l'inflation observée au cours de l'année 2012, quand cette inflation n'était pourtant que purement hypothétique, tant en son principe même qu'en son taux, la cour d'appel a encore méconnu l'article 1382 du code civil, ensemble le principe de réparation intégrale du préjudice sans perte ni profit ; </p>
<p>" 4°) alors que la juridiction pénale ne peut statuer ultra petita sans exposer sa décision à la censure ; qu'en ayant d'office fait application d'un barème de capitalisation de mars 2013, publié à la Gazette du Palais que personne ne réclamait, la cour d'appel a commis un excès de pouvoir ; </p>
<p>" 5°) alors que l'obligation pour le juge pénal, statuant sur les intérêts civils, de respecter le principe de la contradiction s'applique à toute espèce de relevé d'office de moyen de fait ou de droit ; qu'en faisant d'office application du barème de capitalisation édité par la Gazette du Palais en mars 2013, la cour d'appel a méconnu le principe du contradictoire " ; </p>
<p>Attendu que, pour évaluer les pertes de gains professionnels futurs subies par M. Raymond B... à la suite de l'accident dont M. Ernest X..., reconnu coupable de blessures involontaires, a été déclaré tenu à réparation intégrale, les juges du second degré, après avoir déterminé la perte annuelle de ressources, appliquent, pour capitaliser cette somme, le barème édité par la Gazette du palais en mars 2013, lequel tient compte d'un taux de rendement du capital corrigé de l'inflation future ; </p>
<p>Attendu que c'est dans l'exercice de son pouvoir souverain que la cour d'appel, tenue d'assurer la réparation intégrale du dommage actuel et certain de la victime sans perte ni profit, a fait application du barème de capitalisation qui lui a paru le plus adapté à assurer les modalités de cette réparation pour le futur, sans avoir à soumettre ce choix au débat contradictoire ; </p>
<p>D'où il suit que le moyen ne saurait être accueilli ; </p>
<p>Et attendu que l'arrêt est régulier en la forme ; </p>
<p>REJETTE le pourvoi ; </p>
<p>Ainsi fait et jugé par la Cour de cassation, chambre criminelle, et prononcé par le président le cinq avril deux mille seize ; </p>
<p>En foi de quoi le présent arrêt a été signé par le président, le rapporteur et le greffier de chambre.</p>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="1" TYPE="PRINCIPAL">ACTION CIVILE - Préjudice -  Réparation -  Réparation intégrale -  Indemnité -  Préjudice futur -  Evaluation -  Appréciation souveraine des juges du fond -  Barème de capitalisation -  Portée -  Débat contradictoire (non)
</SCT>
<SCT ID="1" TYPE="REFERENCE">INDEMNISATION DES VICTIMES D'INFRACTION -  Indemnité -  Montant -  Fixation -  Réparation intégrale -  Préjudice futur -  Appréciation souveraine des juges du fond -  Barème de capitalisation -  Portée -  Débat contradictoire (non) INDEMNISATION DES VICTIMES D'INFRACTION -  Préjudice -  Réparation -  Réparation intégrale -  Préjudice futur -  Evaluation -  Appréciation souveraine des juges du fond -  Barème de capitalisation -  Portée -  Débat contradictoire (non)
</SCT>
<ANA ID="1">Exerce son pouvoir souverain la cour d'appel qui, tenue d'assurer la réparation intégrale du dommage actuel et certain de la victime sans perte ni profit, fait application du barème de capitalisation lui paraissant le plus adapté à assurer les modalités de cette réparation pour le futur, sans avoir à soumettre ce choix au débat contradictoire</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>Sur l'appréciation souveraine des juges du fond pour choisir un barème de conversion pour fixer la créance de l'Etat, à rapprocher : Crim., 4 février 2003, pourvoi n° 02-81.378, Bull. crim. 2003, n° 23 (rejet), et les arrêts cités 




</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS>
<LIEN cidtexte="" datesignatexte="" id="" naturetexte="" nortexte="" num="" numtexte="" sens="source" typelien="CITATION">article 1382 du code civil ; article 427 du code pénal</LIEN>
</LIENS>
</TEXTE_JURI_JUDI>
